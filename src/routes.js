import React from 'react';
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Users = React.lazy(() => import('./views/Users/Users'));
const UserDetail = React.lazy(() => import('./views/Users/User'));
const AddNewUser = React.lazy(() => import('./views/Users/AddNew'));
const Audio = React.lazy(() => import('./views/Audio/Audios'));
const AddAudio = React.lazy(() => import('./views/Audio/AddNewAudio'));
const EditAudio = React.lazy(() => import('./views/Audio/AddNewAudio'));
const Subscription = React.lazy(() => import('./views/Subscription/Subscription'));
const Plans = React.lazy(() => import('./views/Plans/Plans'));
const AddNewPlan = React.lazy(() => import('./views/Plans/AddNewPlan'));
const CorporateList = React.lazy(() => import('./views/Corporate/Corporates'));
const AddNew = React.lazy(() => import('./views/Corporate/AddNew'));
const EditCorporate = React.lazy(() => import('./views/Corporate/AddNew'));
const category = React.lazy(() => import('./views/Category/Category'));
const DailyAudio = React.lazy(() => import('./views/Daily/DailyAudios'));
const AddNewDailyAudio = React.lazy(() => import('./views/Daily/AddDailyAudio'));
const MeditationMessage = React.lazy(() => import('./views/Message/Message'));


// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/Users', exact: true, name: 'Users', component: Users },
  { path: '/Users/UserDetail/:id', exact: true, name: 'User Details', component: UserDetail },
  { path: '/Users/AddNewUser', exact: true, name: 'Add New User', component: AddNewUser },
  { path: '/Library', exact: true, name: 'Library', component: Audio },
  { path: '/Subscription', exact: true, name: 'Subscriptiondio', component: Subscription },
  { path: '/Library/AddNewAudio', exact: true, name: 'Add New Library', component: AddAudio },
  { path: '/Library/editAudio/:id', exact: true, name: 'Edit Library', component: EditAudio },
  { path: '/Plans', exact: true, name: 'Trial Pack', component: Plans },
  { path: '/Plans/AddNewPlan', exact: true, name: 'Add New Trial Pack', component: AddNewPlan },
  { path: '/Corporates', exact: true, name: 'Corporates', component: CorporateList },
  { path: '/Corporates/AddNew', exact: true, name: 'Add New Corporates', component: AddNew },
  { path: '/Corporates/editCorporate/:id', exact: true, name: 'Edit Corporates', component: EditCorporate },
  { path: '/Category', exact: true, name: 'Category', component: category },
  { path: '/DailyAudio', exact: true, name: 'Daily Audio', component: DailyAudio },
  { path: '/DailyAudio/AddNewDailyAudio', exact: true, name: 'Daily Audio', component: AddNewDailyAudio },
  { path: '/MeditationMessage', exact: true, name: 'Meditation Message', component: MeditationMessage },
];

export default routes;
