import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav } from 'reactstrap';
import { AppSidebarToggler } from '@coreui/react';
import { hashHistory } from 'react-router';
import jwt from 'jsonwebtoken'
import admin from '../../assets/img/icon.png'
import logo from '../../assets/img/logo1.png'
class DefaultHeader extends Component {
  constructor(props) {
    super(props)
    this.state = {
      memberId: '',
      notification: [],
      active: 1,
      defaultactive: 1,
      activeUrl: "dashboard"
    }
  }

  componentDidMount() {
    if (localStorage && localStorage.token && localStorage.token.length > 1) {
      // let tokenDetail = localStorage.token
      var decoded = jwt.decode(localStorage.token);
      this.setState({
        memberId: decoded && decoded.id || ""
      })
    } else {
      hashHistory.push('/login');
    }

    let str = window.location.href
    var n = window.location.href.lastIndexOf('/');
    var result = str.substring(n + 1);
    this.setState({ activeUrl: result })
  }

  addActiveClass(activ) {
    let str = window.location.href
    var n = window.location.href.lastIndexOf('/');
    var result = str.substring(n + 1);
    // this.setState({ activeUrl: result })
    this.setState({
      activeUrl: (activ === "dashboard") ? "dashboard" : (activ === "Users") ? "Users" : (activ === "DailyAudio") ? "DailyAudio" : (activ === "Library") ? "Library" : (activ === "Corporates") ? "Corporates" : (activ === "Category") ? "Category" : (activ === "Plans") ? "Plans" : "MeditationMessage"
    })
  }

  signOut() {
    localStorage.token = ''
    hashHistory.push('/login');
  }

  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    let str = window.location.href
    var n = window.location.href.lastIndexOf('/');
    var result = str.substring(n + 1);
    this.state.activeUrl = result

    // const tokenDetail = jwt.decode(config.token);

    // if (tokenDetail && tokenDetail.exp && tokenDetail.exp < (Date.now() / 1000)) {
    //   localStorage.token = ''
    //   hashHistory.push('/login');
    // }

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <NavLink to={`/dashboard`}>
          {/*  <AppNavbarBrand
          full={{ src: logo, width: 100, height:50, alt: 'MindTree' }}
          minimized={{ src: sygnet, width: 45, alt: 'MindTree' }}
        /> */}
          <span style={{ marginLeft: '15px', fontSize: '25px', fontWeight: 'bold', color: 'black' }}><img src={logo} alt="mindtree" style={{ width: '150px', paddingBottom: '15px' }}></img></span>
        </NavLink>
        {/* <AppSidebarToggler className="d-md-down-none" display="lg" /> */}

        <Nav className="ml-auto" navbar>
          <li className="nav-item menuclass">
            <a aria-current="page" className={(this.state.activeUrl && this.state.activeUrl === "dashboard") ? "nav-link active" : "nav-link"} href="#/dashboard" onClick={() => { this.addActiveClass("dashboard") }}>
              Dashboard</a>
          </li>
          <li className="nav-item menuclass">
            <a className={(this.state.activeUrl && this.state.activeUrl === "Users") ? "nav-link active" : "nav-link"} href="#/Users" onClick={() => { this.addActiveClass("Users") }}>
              Users</a></li>

          <li className="nav-item menuclass">
            <a className={(this.state.activeUrl && this.state.activeUrl === "DailyAudio") ? "nav-link active" : "nav-link"} href="#/DailyAudio" onClick={() => { this.addActiveClass("DailyAudio") }}>
              Daily</a></li>
          <li className="nav-item menuclass">
            <a className={(this.state.activeUrl && this.state.activeUrl === "Library") ? "nav-link active" : "nav-link"} href="#/Library" onClick={() => { this.addActiveClass("Audio") }}>
              Library</a></li>

          {/* <li class="nav-item menuclass" >
            <a class={(this.state.active === 5) ? "nav-link active" : "nav-link"} href="#/Audio" onClick={() => { this.addActiveClass(3) }}>
                        Audio</a></li> */}
          <li className="nav-item menuclass">
            <a className={(this.state.activeUrl && this.state.activeUrl === "Corporates") ? "nav-link active" : "nav-link"} href="#/Corporates" onClick={() => { this.addActiveClass("Corporates") }}>
              Corporate</a></li>
          <li className="nav-item menuclass">
            <a className={(this.state.activeUrl && this.state.activeUrl === "Category") ? "nav-link active" : "nav-link"} href="#/Category" onClick={() => { this.addActiveClass("Category") }}>
              Category</a>
          </li>
          <li className="nav-item menuclass">
            <a className={(this.state.activeUrl && this.state.activeUrl === "Plans") ? "nav-link active" : "nav-link"} href="#/Plans" onClick={() => { this.addActiveClass("Plans") }}>
              Trial Pack</a>
          </li>
          <li className="nav-item menuclass" style={{ marginRight: '110px' }}>
            <a className={(this.state.activeUrl && this.state.activeUrl === "MeditationMessage") ? "nav-link active" : "nav-link"} href="#/MeditationMessage" onClick={() => { this.addActiveClass("MeditationMessage") }}>
              Meditation Message</a>
          </li>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <img src={admin} className="img-avatar" alt="MindTree" />
            </DropdownToggle>
            <DropdownMenu right>
              {/*  <DropdownItem><NavLink to={`/EditProfile/${this.state.memberId}`} className="nav-link"><i className="fa fa-user"></i> Edit Profile</NavLink></DropdownItem>
              <DropdownItem><NavLink to={`/ChangePassword`} className="nav-link"><i className="fa fa-key"></i> Change Password</NavLink></DropdownItem> */}
              <DropdownItem><span onClick={this.signOut} style={{ cursor: 'pointer' }}>Sign Out</span></DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

export default DefaultHeader;
