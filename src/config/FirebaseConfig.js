import firebase from 'firebase'
var firebaseConfig = {
  apiKey: "AIzaSyBPq6C2t78HVlM1mXWLB8sYdFn83pLtr5s",
  authDomain: "breeze-react-native.firebaseapp.com",
  databaseURL: "https://breeze-react-native.firebaseio.com",
  projectId: "breeze-react-native",
  storageBucket: "breeze-react-native.appspot.com",
  messagingSenderId: "236013736792",
  appId: "1:236013736792:web:f78d1a2f43401e17890b2d",
  measurementId: "G-9BQJMRWV5Q"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

export default firebase