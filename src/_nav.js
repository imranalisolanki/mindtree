export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },
    {
      name: 'Users',
      url: '/Users',
      icon: 'icon-user',
    },
    {
      name: 'Audio',
      url: '/Audio',
      icon: 'fa fa-volume-up',
      children: [
        {
          name: 'View Audio',
          url: '/Audio',
          icon: 'icon-list',
        },
        {
          name: 'Add New Audio',
          url: '/Audio/AddNewAudio',
          icon: 'icon-plus',
        }
      ]
    },
    {
      name: 'Medication Message',
      url: '/MedicationMessage',
      icon: 'icon-message',
    },
  ],
};
