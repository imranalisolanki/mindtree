import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../scss/vendors/bootstrap-pagination.css';
import { Card, CardBody, CardHeader, Col, Row, Badge } from 'reactstrap';
import FilterableTable from 'react-filterable-table'
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios'
import AppConfig from '../../config/appConfig'
import qs from 'qs'
import _ from 'lodash'
import moment from 'moment'
const getBadge = (status) => {
  return status === 'Active' ? 'success' :
    status === 'Inactive' ? 'secondary' :
      status === 'Pending' ? 'warning' :
        status === 'Banned' ? 'danger' :
          'primary'
}


class Corporates extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startAt: 0,
      currentPage: 1,
      activePage: 1,
      itemsPerPage: 10,
      limit: 10,
      skip: 0,
      audioList: []
    }
    this.changeCorporateStatus = this.changeCorporateStatus.bind(this)
  }
  componentDidMount() {
    const pageNumber = 1
    this.getUsersListData(pageNumber)

  }

  getUsersListData(pageNumber) {
    const { itemsPerPage } = this.state
    const activePage = pageNumber
    // const sortOrder = event
    this.setState({ activePage })
    const startAt = pageNumber * itemsPerPage - itemsPerPage
    var cond = {
      filter: {
        include: [
          { relation: "plan" }
        ],
        order: ['created DESC']
      }
    }
    const query = qs.stringify(cond, { addQueryPrefix: true })
    axios.get(AppConfig.ApiUrl + `corporates` + query, AppConfig.headerConfig).then(res => {
      if (res && res.data) {
        const audioList = (res && res.data) ? res.data : []

        this.setState({
          audioList, startAt
        })
      } else {
        this.setState({
          audioList: []
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }
  changeCorporateStatus = (status, actv, id) => {

    let cnd = window.confirm('Are you sure you want to ' + actv + " Corporate Plan?")
    if (cnd) {
      let payload = {
        status: status
      }
      axios.patch(AppConfig.ApiUrl + `corporates/${id}`, payload).then(res => {
        if (res) {
          toast.success("Updated sucessfully!")
          setTimeout(() => {
            this.getUsersListData()
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
      })
    } else {
      // console.log('else part is here')
    }
  }
  render() {
    let _this = this
    var data = [];
    // var pgNo = this.state.startAt;

    _.forEach(this.state.audioList, function (val, index) {
      var res = (val && val.plan && val.plan.planType) ? val.plan.planType.slice(0, 2) : 0;
      if (res == 'Li') {
        var expire = moment(val.created).add(50, 'years').format('DD-MM-YYYY')
      } else {
        var expire = moment(val.created).add(+res, 'months').format('DD-MM-YYYY')
      }

      data.push({
        corporate: <Link to={`/Corporates/editCorporate/${val.id}`}>{val.corporate}</Link>,
        name: val.name,
        email: (val && val.email) ? val.email : "",
        mobile: val.mobile,
        employees: (val && val.plan && val.plan.employees) ? val.plan.employees : 0,
        status: (val && val.status === 0) ? <span style={{ cursor: 'pointer' }} onClick={(e) => { _this.changeCorporateStatus(1, 'Inactive', val.id) }}><Badge color={getBadge('Active')}>Active</Badge></span> : <span style={{ cursor: 'pointer' }} onClick={(e) => { _this.changeCorporateStatus(0, "Active", val.id) }}><Badge color={getBadge('Inactive')}>Inactive</Badge></span>,
        expire: expire,

      })
    })

    const fields = [
      { name: 'corporate', displayName: "Corporate ", inputFilterable: true, sortable: true },
      { name: 'name', displayName: "Name", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'email', displayName: "Email", inputFilterable: false, exactFilterable: false, sortable: true },
      { name: 'mobile', displayName: "Mobile", inputFilterable: false, exactFilterable: false, sortable: true },
      { name: 'employees', displayName: "Employees", inputFilterable: false, exactFilterable: false, sortable: true },
      { name: 'status', displayName: "status", inputFilterable: true, sortable: true },
      { name: 'expire', displayName: "Expires On", inputFilterable: true, sortable: true },

    ];
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Corporates <small className="text-muted">List</small>
                <Link to={`/Corporates/AddNew`} style={{ float: 'right' }}> <button class="btn btn-primary" style={{ float: "right" }}> Add New</button></Link>
              </CardHeader>
              <CardBody>
                <FilterableTable
                  namespace="Corporate"
                  initialSort="created"
                  topPagerVisible={false}
                  data={data}
                  fields={fields}
                  noRecordsMessage="There are no Corporates to display"
                  noFilteredRecordsMessage="Corporates not found!"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
        <ToastContainer />
      </div>
    )
  }
}

export default Corporates;
