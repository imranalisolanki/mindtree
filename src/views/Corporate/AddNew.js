import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, FormGroup, Input, Label, Button, Form } from 'reactstrap';
import * as _ from 'lodash'
// import  request = require('request');
import request from 'request'
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { hashHistory } from 'react-router'
import axios from 'axios'
// import qs from 'qs'
import AppConfig from '../../config/appConfig'
import { func } from 'prop-types';
class AddNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      planList: [],
      selected: true,
      planId: "",
      mobile: "",
      city: "",
      address: "",
      email: "",
      emails: "",
      name: "",
      corporate: {},
      corporates: "",
      selectedId: '',
      employess: [],
      type: null,
      status: "",
      emError: "",
      openPopUp: true,
      rmIndex: '',
      saveButton: false,
      message: "",
      isTrue: true,
      planType: null,
      employeeNo: null,
      showUpdate: false
    }
    this.submit = this.submit.bind(this)
    this.getOnChangeValue = this.getOnChangeValue.bind(this)
    this.getInputChange = this.getInputChange.bind(this)
    this.AddNewEmployeeForm = this.AddNewEmployeeForm.bind(this)
    this.createDyanicLinks = this.createDyanicLinks.bind(this)
  }
  componentDidMount() {
    // this.createDyanicLinks()
    if (this.props && this.props.match && this.props.match.params.id) {
      this.getCorporateDetailById()

    } else {
      this.setState({
        isTrue: false
      })
      this.getPlanList()
      // this.state.isTrue = false
    }

  }
  AddNewEmployeeForm() {
    this.setState({
      openPopUp: false
    })
  }
  getOnChangeValue(e) {
    if (e.target.value && e.target.value.length > 0) {
      this.setState({
        [e.target.name]: e.target.value,
        saveButton: true

      })
    } else {
      this.setState({
        saveButton: false
      })
    }
  }

  getInputChange(e) {
    if (e.target.value && e.target.value.length > 0) {
      this.setState({
        [e.target.name]: e.target.value,
        saveButton: true,
        showUpdate: true
      })
      if (this.props && this.props.match && this.props.match.params.id) {
        this.setState({
          showUpdate: true
        })
      }
    } else {
      this.setState({
        saveButton: false,
        showUpdate: false
      })
    }
  }
  getCorporateDetailById() {
    axios.get(AppConfig.ApiUrl + `corporates/` + this.props.match.params.id).then(res => {
      if (res && res.data) {
        // console.log(res.data, '==============-=-=-=-', res.data.plan.employees, '========8888')
        this.state.type = res && res.data && res.data.plan && res.data.plan.employess || ""
        this.setState({
          corporate: res && res.data || {},
          selectedId: res && res.data && res.data.plan && res.data.plan.id || "",
          employess: res && res.data && res.data.employess || "",
          planType: res && res.data && res.data.employess && res.data.employess.length && res.data.employess[0] && res.data.employess[0].status,
          type: res && res.data && res.data.plan && res.data.plan.employees || ""
        })

      }
    }).catch(err => {
      console.log(err)
    })
  }
  getPlanList() {
    axios.get(AppConfig.ApiUrl + `plans`).then(res => {
      if (res && res.data) {

        this.setState({
          planList: res && res.data || [],
          planId: res && res.data && res.data[0] && res.data[0].id || "",
          type: res && res.data && res.data[0] && res.data[0].employees || "",
          planType: res && res.data && res.data[0] && res.data[0].type || "",

        })
      }
    }).catch(err => {
      console.log(err)
    })
  }
  planSlected(id, type, planType) {
    if (this.props && this.props.match && this.props.match.params.id) {

    } else {
      this.setState({
        planId: id,
        type: type,
        planType: planType
      })
    }
  }

  removeEmployee() {
    let _this = this
    if (_this.state.rmIndex !== '') {
      var someArray = _.filter(this.state.employess, function (val, index) {
        return index != _this.state.rmIndex
      })
      if (this.props && this.props.match && this.props.match.params.id) {
        if (this.state.employess && this.state.employess.length) {
          let removeUser = this.state.employess && this.state.employess.length && this.state.employess[_this.state.rmIndex] || {}
          if (removeUser && removeUser.email) {
            let payload = {
              email: removeUser.email,
              corporateId: this.props.match.params.id,
              employess: someArray
            }

            axios.post(AppConfig.ApiUrl + `corporates/deleteSubscriptionPlan`, payload, AppConfig.headerConfig).then(res => {
              if (res && res.status === 204) {
                toast.success("Employee remove sucessfully!")
              } else {
                alert("No subscription found")
              }
            }).catch(err => {
              toast.success("Employee remove sucessfully!")
              console.log(err)
            })
          }
        }
      }
      this.setState({
        employess: someArray
      })
    }

  }
  SelectremoveEmployee(index) {
    this.setState({
      rmIndex: index
    })

  }
  submit(event) {
    event.preventDefault()
    let emply = []
    let _this = this

    let email = this.state.email && this.state.email !== '' && this.state.email.split(",")
    email.slice(0, email.indexOf("@"));

    _.forEach(email && email.length && email, function (val) {

      if (_this.state.employess && _this.state.employess.length) {
        _this.state.employess.push({
          name: val.slice(0, val.indexOf("@")),
          email: val,
          status: _this.state.planType
        })
      } else {
        emply.push({
          name: val.slice(0, val.indexOf("@")),
          email: val,
          status: _this.state.planType
        })
      }

    })

    if (this.props && this.props.match && this.props.match.params.id) {

      if (_this.state.employess && _this.state.employess.length) {

      } else {
        this.state.employess = emply
      }
    } else {
      this.state.employess = emply
    }

    if (_this.state.employess && _this.state.employess.length > this.state.type) {
      this.getCorporateDetailById();
      alert("Employees should not greater than selected plan");
      this.setState({
        emError: "Employees should not greater than selected plan"
      })
    } else {

      var payload = {
        mobile: this.state.mobile != "" && this.state.mobile || this.state.corporate && this.state.corporate.mobile,
        city: this.state.city != "" && this.state.city || this.state.corporate && this.state.corporate.city,
        address: this.state.address != "" && this.state.address || this.state.corporate && this.state.corporate.city,
        email: this.state.emails != "" && this.state.emails || this.state.corporate && this.state.corporate.email,
        name: this.state.name != "" && this.state.name || this.state.corporate && this.state.corporate.name,
        corporate: this.state.corporates != "" && this.state.corporates || this.state.corporate && this.state.corporate.corporates,
        employess: this.state.employess,
        planId: this.state.selectedId != "" && this.state.selectedId || this.state.planId,
        userId: "5ea01990bbb07a27a0b130e0"
      }

      if (this.props && this.props.match && this.props.match.params.id) {
        axios.patch(AppConfig.ApiUrl + `corporates/` + this.props.match.params.id, payload).then(res => {
          if (res) {
            // let email = "gotam@amplework.com"
            this.createDyanicLinks(this.props.match.params.id, email, this.state.message)

          }
        }).catch(err => {
          console.log(err)
        })
      } else {
        axios.post(AppConfig.ApiUrl + `corporates`, payload).then(res => {
          if (res && res.data) {
            toast.success("Corporates Added sucessfully!")
            this.createDyanicLinks(res.data.id, email, this.state.message)
          }
        }).catch(err => {
          console.log(err)
        })
      }
    }
  }

  createDyanicLinks(id, email, msg) {
    let _this = this
    const API_KEY = "AIzaSyC431hpD1G030V2VW9s-YhDqo8-Iv7NoSk";

    if (email && email.length) {
      _.forEach(email, function (val, index) {
        const body = {
          'dynamicLinkInfo': {
            'domainUriPrefix': 'https://mindtreeample1.page.link',
            'link': 'https://www.example.com/?id=' + id + '&email=' + val,
            "androidInfo": {
              "androidPackageName": "com.ample.mindtree"
            },
            "iosInfo": {
              "iosBundleId": "com.amplework.mindtree",
              "iosAppStoreId": "1526938919"
            }
          }
        }

        request({
          url: `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${API_KEY}`,
          method: 'POST', json: true, body
        }, function (error, response) {
          if (error) {
            console.log('Error :', error)
          } else {
            if (response && response.statusCode !== 200) {
              console.log('Error on Request :', response.body.error.message)
            } else {
              let payload = {
                corporateId: id,
                email: val,
                link: response.body && response.body.shortLink,
                msg: msg
              }

              axios.post(AppConfig.ApiUrl + `corporates/sendEmployeeInvitation`, payload, AppConfig.headerConfig).then(ress => {
                if (ress) {
                  if (email.length === index + 1) {
                    toast.success("Employee added sucessfully!")
                    _this.setState({ message: '', email: "", status: '' })
                    setTimeout(() => {
                      hashHistory.push('/Corporates')
                    }, 1000);
                  }
                }
              }).catch(err => {
                console.log(err)
              })
            }
          }
        });
      })
    } else {
      _this.setState({ message: '', email: "", status: '' })
      toast.success("Corporates Added sucessfully!")
      setTimeout(() => {
        hashHistory.push('/Corporates')
      }, 1000);
    }
  }
  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col className="corporates" >
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> {(this.props.match && this.props.match.params.id) ? "View Corporate" : "Add New Corporate"}  <small className="text-muted"></small>

              </CardHeader>
              <Form autocomplete="off" onSubmit={this.submit} enctype='multipart/form-data' style={{ paddingBottom: "20px" }}>
                <CardBody style={{ height: "300px" }}>
                  <Col xl={6} style={{ float: "left" }} >
                    <FormGroup>
                      <Label>Corporate</Label>
                      <Input type="text" id="corporate" name="corporates" required placeholder="Corporate Name" defaultValue={this.state.corporate && this.state.corporate.corporate} onChange={this.getInputChange} />
                    </FormGroup>
                  </Col>
                  <Col xl={6} style={{ float: "right" }}>
                    <FormGroup>
                      <Label>Contact Person</Label>
                      <Input type="text" id="name" name="name" required placeholder="Contact Person" defaultValue={this.state.corporate && this.state.corporate.name} onChange={this.getInputChange} />
                    </FormGroup>
                  </Col>
                  <Col xl={6} style={{ float: "left" }} >
                    <FormGroup>
                      <Label>Address</Label>
                      <Input type="text" id="address" name="address" required placeholder="Address" defaultValue={this.state.corporate && this.state.corporate.address} onChange={this.getInputChange} />
                    </FormGroup>
                  </Col>
                  <Col xl={6} style={{ float: "right" }}>
                    <FormGroup>
                      <Label>Email</Label>
                      <Input type="text" id="email" name="emails" required placeholder="Email" defaultValue={this.state.corporate && this.state.corporate.email} onChange={this.getInputChange} />
                    </FormGroup>
                  </Col>
                  <Col xl={6} style={{ float: "left" }} >
                    <FormGroup>
                      <Label>City</Label>
                      <Input type="text" id="city" name="city" required placeholder="City" defaultValue={this.state.corporate && this.state.corporate.city} onChange={this.getInputChange} />
                    </FormGroup>
                  </Col>
                  <Col xl={6} style={{ float: "right" }}>
                    <FormGroup>
                      <Label>Mobile</Label>
                      <Input type="text" id="mobile" name="mobile" required placeholder="Mobile" defaultValue={this.state.corporate && this.state.corporate.mobile} onChange={this.getInputChange} />
                    </FormGroup>
                  </Col>
                  {
                    (this.state.showUpdate) ?
                      <Col xl={3}>
                        <Button type="submit" class="btn btn-primary" color="primary">Update</Button>
                      </Col>
                      : ""
                  }
                </CardBody>
                {
                  (this.state.planList && this.state.planList.length) ?
                    <div className="plans" style={{ height: "280px", paddingTop: '15px', backgroundColor: "#eaeaea" }}>
                      <Label htmlFor="company" style={{ marginLeft: '20px' }}><b>Select Plan</b> </Label><br></br><br></br>
                      {
                        this.state.planList && this.state.planList.length && this.state.planList.map((plan, index) =>

                          <Col xl={3} className={(plan.id === this.state.selectedId) ? "selected   corpbor corpo" + index : (plan.id === this.state.planId && this.state.selectedId === "") ? "activeSelected  corpbor corpo" + index : "corpbor corpo" + index} style={{ float: "left", backgroundColor: "#9ad9ea", marginLeft: '20px', marginBottom: '20px', width: "15%", cursor: "pointer" }} onClick={() => { this.planSlected(plan.id, plan.employees, plan.type) }}>
                            <p>{plan.type}</p>
                            <p>Upto{plan.employees} <br></br> <b>Employees</b></p>
                            <p>{plan.planType}</p>
                            <p>Premium : Access</p>
                          </Col>
                        )
                      }

                    </div>
                    : ""
                }
                {
                  (this.state.employess && this.state.employess.length) ?
                    <div className="employessData" style={{ height: this.state.employess && this.state.employess.length && this.state.employess.length > 4 && (this.state.employess.length * 47) + "px" || "350px" }}>
                      <Col xl={12}>
                        <ul style={{ width: '100%', float: 'left', listStyle: 'none', paddingBottom: '20px', paddingTop: "20px" }}>
                          <p style={{ marginLeft: '-40px' }}><b>Employees Status</b></p>
                          {
                            this.state.employess && this.state.employess.length && this.state.employess.map((val, index) =>

                              <li style={{ paddingBottom: '20px' }} onClick={() => { this.SelectremoveEmployee(index) }}>
                                <div>
                                  <Input type="radio" id={"radio" + index} name="rad" style={{ marginLeft: "10px" }} />
                                </div>
                                <div style={{ marginLeft: "60px", float: 'left', width: '15%', fontWeight: '500' }}> {val.name}</div>
                                <div style={{ marginLeft: "33px", float: 'left', width: '30%', fontWeight: '500' }}>{val.email}</div>
                                <div style={{ marginLeft: "102px", fontWeight: '500' }} className={(index === 0) ? "listed" + index : ""}>{val.status}</div>
                              </li>

                            )
                          }
                          <span class="btn" style={{ marginTop: "10px", border: '1px solid black', marginLeft: "20px", cursor: 'pointer' }} onClick={() => { this.removeEmployee() }}>Remove</span>
                          {
                            (this.state.openPopUp === true) ?
                              <span class="btn" style={{ marginTop: "10px", border: '1px solid black', marginLeft: '15px', cursor: 'pointer' }} onClick={this.AddNewEmployeeForm}>Add Employees</span>
                              : ""
                          }

                        </ul>
                      </Col>
                    </div>
                    :
                    (this.props && this.props.match && this.props.match.params.id) ?
                      <div className="employessData" style={{ height: this.state.employess && this.state.employess.length && this.state.employess.length > 4 && "650px" || "150px" }}>
                        <Col xl={12}>
                          <ul style={{ width: '100%', float: 'left', listStyle: 'none', paddingBottom: '20px', paddingTop: "20px" }}>
                            <span class="btn" style={{ marginTop: "10px", border: '1px solid black', marginLeft: '15px' }} onClick={this.AddNewEmployeeForm}>Add Employees</span>
                          </ul>
                        </Col>
                      </div>
                      : ""
                }
                <div className={(!this.props.match.params.id && this.state.openPopUp === true) ? "openActive" : (this.props.match.params.id && this.state.openPopUp === false) ? "openActive" : "closeactive"}>
                  <Col xl={12}>
                    <p style={{ paddingTop: '10px' }}><b>Invite Employees</b></p>
                    <FormGroup>
                      <Input type="text" id="location" defaultValue={this.state.email} name="email" required={(this.state.employess && this.state.employess.length) ? false : true} placeholder="Add Employess Email'Id Separated by comma" onChange={this.getOnChangeValue} />
                    </FormGroup>
                  </Col>
                  <Col xl={12}>
                    <FormGroup>
                      <Input type="textarea" name="message" id="textarea-input" rows="9" defaultValue={this.state.message}
                        placeholder="Add Msg for Your employess" required={(this.state.employess && this.state.employess.length) ? false : true} onChange={this.getOnChangeValue} />
                    </FormGroup>
                    <FormGroup>
                      <Input type="text" id="status" readOnly={true} value={this.state.employess && this.state.employess.length && this.state.employess[0] && this.state.employess[0].status || this.state.planType} name="status" required={(this.state.employess && this.state.employess.length) ? false : true} placeholder="status" />
                    </FormGroup>
                  </Col>
                </div>
                {
                  (this.props.match.params.id && this.state.openPopUp === false) ?
                    <Col xl={3} style={{ paddingTop: "20px", }}>
                      <Button type="submit" class="btn btn-primary" color="primary">Save & Invite</Button>
                    </Col>
                    : ""
                }
                {
                  (!this.props.match.params.id && this.state.openPopUp === true) ?
                    <Col xl={3} style={{ paddingTop: "20px", }}>
                      <Button type="submit" class="btn btn-primary" color="primary">Save & Invite</Button>
                    </Col>
                    : ""
                }
              </Form>
            </Card>
          </Col>
        </Row>
        <ToastContainer />
      </div>
    );
  }
}
// #efe3af
export default AddNew;
