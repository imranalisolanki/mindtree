import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { MultipleSelect } from "react-select-material-ui";
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import { Card, CardBody, Container, Col, Row, Form, FormGroup, Input, Label, Button } from 'reactstrap';
import S3FileUpload from 'react-s3'
import jwt from 'jsonwebtoken'
import axios from 'axios'
import qs from 'qs'
import _ from 'lodash'
import moment from 'moment'
import AppConfig from '../../config/appConfig'
import { hashHistory } from 'react-router'
const decoded = jwt.decode(localStorage.token);

const config = {
  bucketName: 'mindtree1',
  dirName: 'audio', /* optional */
  region: 'us-east-2',
  accessKeyId: AppConfig.accesskey,
  secretAccessKey: AppConfig.secretKey,
}

class AddNewAudio extends Component {
  constructor(props) {
    super(props)
    this.state = {
      input: {
        display: 'none'
      },
      custom: {
        border: '1px solid #ccc',
        display: 'inline - block',
        padding: '6px 12px',
        cursor: 'pointer'
      },
      banner: null,
      track: null,
      trackImage: null,
      loader: false,
      disable: true,
      load: false,
      loadr: false,
      audioList: [],
      name: '',
      name0: '',
      name1: '',
      name2: '',
      name3: '',
      name4: '',
      name5: '',
      name6: '',
      image: '',
      type: '',
      userData: {},
      categoryList: [],
      selectedCat: [],
      inputs: [],
      cat: [],
      isLocked: true,
      audioData: [],
      resultData: {},
      selectOption: ["single", "multiple", "courses"],
      existPosition: null,
      audioId: null,
      position: null,
      newPosition: null,
    }

    this.submit = this.submit.bind(this)
    this.handleOnChange = this.handleOnChange.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.checkAudioPosition = this.checkAudioPosition.bind(this)
  }

  componentDidMount() {
    const { match } = this.props
    if (match && match.params && match.params.id) {

      axios.get(AppConfig.ApiUrl + `audio/${match.params.id}`, AppConfig.headerConfig).then(res => {
        if (res && res.data) {
          let selectOption = _.filter(this.state.selectOption, function (val) {
            return val !== res.data.type
          })

          this.setState({
            selectOption: selectOption,
            userData: (res && res.data) ? res.data : {},
            cat: (res && res.data && res.data.categoryId) ? res.data.categoryId : [],
            isLocked: (res && res.data && res.data.type) ? res.data.isLocked : "",
            type: (res && res.data && res.data.type) ? res.data.type : "",
            name: (res && res.data && res.data.name) ? res.data.name : "",
            existPosition: (res && res.data && res.data.position) ? res.data.position : "",
            position: (res && res.data && res.data.position) ? res.data.position : "",
            audioData: (res && res.data && res.data.audio) ? res.data.audio : []
          })

          this.getCategoryData()
        }
      }).catch(err => {
        console.log(err)
      })
    }
  }
  getCategoryData() {
    var cond = {
      filter: {
        order: ['created DESC']
      }
    }

    const query = qs.stringify(cond, { addQueryPrefix: true })
    axios.get(AppConfig.ApiUrl + `categories` + query, AppConfig.headerConfig).then(res => {
      if (res && res.data) {
        // let cat = (res && res.data) ? res.data : []
        let catName = []
        _.forEach(res && res.data, function (val) {
          catName.push(val.name)
        })
        this.setState({
          categoryList: catName
        })
      } else {
        this.setState({ categoryList: [] })
      }
    }).catch(err => {

      console.log(err)
    })
  }
  handleChange(e) {
    console.log(e, '======')
    if (e && e.length) {
      e.concat(e)

      this.setState({ cat: e, disable: false })
    } else {
      this.setState({ cat: '', disable: false })
    }
  }
  _handleImageChange(e) {
    this.setState({ load: true, disable: true })
    var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
    const file = "audioBannerImage" + currentDate + e.target.files[0].name;
    var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
    var newFile = new File([filesObject], file, { type: e.target.files[0].type });

    S3FileUpload
      .uploadFile(newFile, config)
      .then(data => {

        this.setState({ banner: data.location, load: false, disable: false })
      })
      .catch(err => console.error(err))
  }

  // _handleBgImageChange(e) {
  //   this.setState({ loadr: true, disable: true })
  //   var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
  //   const file = "audioBannerImage" + currentDate + e.target.files[0].name;
  //   var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
  //   var newFile = new File([filesObject], file, { type: e.target.files[0].type });

  //   S3FileUpload
  //     .uploadFile(newFile, config)
  //     .then(data => {
  //       this.setState({
  //         trackImage: data.location, loadr: false, disable: false
  //       })
  //     }).catch(err => {
  //       console.log(err, '------------')
  //     })
  // }

  _handleAudioChange(e, index) {

    var imageList = []
    let result = {}
    let audioList = this.state.audioData
    this.setState({
      loader: true, disable: true
    })
    if (this.state.resultData && this.state.resultData.title) {
      audioList.concat(this.state.audioList)
    }
    var currentDate = moment().unix()
    const file = "audio" + currentDate + e.target.files[0].name;
    var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
    var newFile = new File([filesObject], file, { type: e.target.files[0].type });
    S3FileUpload
      .uploadFile(newFile, config)
      .then(data => {

        result = audioList[index]
        result.audio = data.location
        audioList.concat(result)
        this.setState({
          audioData: audioList, loader: false, disable: false
        })
      }).catch(err => console.error(err))
    // })
    const image = this.state.audioList
    this.setState({ image })
  }
  handleOnChange(e) {
    if (e.target.value != '') {
      this.setState({
        [e.target.name]: e.target.value,
        disable: false
      })
      if (e.target.name == "position") {
        this.setState({ existPosition: e.target.value })
        this.checkAudioPosition(e.target.value)
      }
    } else {
      this.setState({ disable: true, existPosition: '' })
    }

  }
  checkAudioPosition(position) {
    let payload = {
      filter: {
        where: {
          position: position
        }
      }
    }
    payload = qs.stringify(payload, { addQueryPrefix: true })
    axios.get(AppConfig.ApiUrl + `audio${payload}`).then(res => {
      if (res && res.status == 200) {

        let data = res && res.data && res.data.length && res.data[0] || {}
        let position = data && data.position || null

        this.setState({
          newPosition: position,
          audioId: data && data.id || null
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }
  handleAudioTitleOnChange(e, index) {

    let data = this.state.audioData[index]
    data.title = e.target.value
    this.setState({
      resultData: data,
      disable: false
    })
  }
  handleAudioSizeOnChange(e, index) {

    let data = this.state.audioData[index]
    data.size = e.target.value
    this.setState({
      resultData: data,
      disable: false
    })
  }
  submit(event) {
    event.preventDefault()
    let payload = {
      categoryId: this.state.cat && this.state.cat.length && this.state.cat || [],
      name: this.state.name,
      isLocked: (this.state.isLocked == "yes") ? true : false,
      position: Number(this.state.position),
      type: event.target.type.value,
      bannerImage: (this.state.banner == null) ? this.state.userData.bannerImage : this.state.banner,
      audio: this.state.audioData && this.state.audioData.length && this.state.audioData || [],
      userId: decoded && decoded.id || "5ea01990bbb07a27a0b130e0",
      // modiefy: moment().toISOString()
    }

    const { match } = this.props

    if (match && match.params && match.params.id) {

      if (this.state.audioId != null && match.params.id != this.state.audioId) {
        axios.delete(AppConfig.ApiUrl + `audio/${this.state.audioId}`, AppConfig.headerConfig).then(res => {
          if (res && res.status == 204) {
          }
        }).catch(err => {
          console.log(err)
        })
      }

      axios.patch(AppConfig.ApiUrl + `audio/${match.params.id}`, payload).then(res => {
        if (res) {
          toast.success("Updated sucessfully!")
          this.setState({ position: null, audioId: null })
          setTimeout(() => {
            hashHistory.push('/Library')
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
      })
    }
  }

  appendInput() {
    var newInput = `input-${this.state.inputs.length}`;
    this.setState(prevState => ({ inputs: prevState.inputs.concat([newInput]) }));
  }

  render() {
    let _this = this

    return (
      <div className="app  align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8" xl="8">
              <Card className="mx-2">
                <CardBody className="p-4">
                  <Form onSubmit={this.submit} encType="multipart/form-data">

                    <FormGroup className="mb-3">
                      <MultipleSelect
                        label="Choose Category"
                        values={this.state.cat && this.state.cat.length && this.state.cat || ""}
                        options={this.state.categoryList}
                        onChange={this.handleChange}
                      />

                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label><b>File Type</b></Label>
                      <Input type="select" name="type" id="exampleSelect" defaultValue={this.state.userData && this.state.userData.type} onChange={this.handleOnChange} required>
                        <option>{(this.state.userData && this.state.userData.type) ? this.state.userData.type : 'Session'}</option>
                        {
                          this.state.selectOption.map((option) =>
                            <option value={option}>{option}</option>
                          )
                        }
                      </Input>
                    </FormGroup>

                    <FormGroup className="mb-3">
                      <Label><b>Locked Audio</b></Label>
                      {
                        (this.state.isLocked) ?
                          <Input type="select" value={this.state.isLocked} name="isLocked" id="exampleSelect" onChange={this.handleOnChange} required>
                            <option key='yes' value="yes">Yes</option>
                            <option key='no' value="no">No</option>
                          </Input>
                          :
                          <Input type="select" value={this.state.isLocked} name="isLocked" id="exampleSelect" onChange={this.handleOnChange} required>
                            <option key='no' value="no">No</option>
                            <option key='yes' value="yes">Yes</option>
                          </Input>
                      }


                    </FormGroup>

                    <FormGroup className="mb-3">
                      <Label><b>Audio Position</b></Label>
                      <Input type="number" name="position" placeholder="Audio Position" autoComplete="position" required defaultValue={(this.state.existPosition == 0) ? 0 : this.state.existPosition} onChange={this.handleOnChange} />
                    </FormGroup>

                    <FormGroup className="mb-3">
                      <Label><b>Upload Banner Image</b></Label>
                      {
                        (this.state.load) ?
                          <Loader
                            type="TailSpin"
                            color="#00BFFF"
                            height={50}
                            width={50}
                            class="loader"
                            timeout={150000000}
                          />
                          : ""
                      }
                      {/*  <Input type="file" placeholder="Image" autoComplete="Image" onChange={(e) => this._handleImageChange(e)} /> */}

                      <br></br>
                      <label for="file-upload1" class="custom-file-upload" style={_this.state.custom} >
                        <i class="fa fa-cloud-upload"></i> Choose file
                            </label>
                      <br></br>
                      <Input id="file-upload1" type="file" accept="image/*" style={_this.state.input} onChange={(e) => this._handleImageChange(e)} />
                      <br></br>
                      {
                        (this.state.userData && this.state.userData.bannerImage) ?
                          <img src={this.state.userData.bannerImage} style={{ height: '50px', width: '100px' }} alt="audio"></img>
                          : ""
                      }
                    </FormGroup>

                    {
                      this.state.userData && this.state.userData.audio && this.state.userData.audio.length && this.state.userData.audio.map((val, index) =>
                        <div>
                          <FormGroup className="mb-3">
                            <Label><b>Track Title</b></Label>
                            <Input type="text" name="name" placeholder="Title" autoComplete="Name" required defaultValue={val && val.title || ""} onChange={(e) => { this.handleAudioTitleOnChange(e, index) }} />
                          </FormGroup>
                          <FormGroup className="mb-3">
                            <Label><b>Audio Duration</b></Label>
                            <Input type="text" name="size" placeholder="Audio Duration" autoComplete="Size" required defaultValue={val && val.size || ""} onChange={(e) => { this.handleAudioSizeOnChange(e, index) }} />
                          </FormGroup>
                          <FormGroup className="mb-3">

                            <Label><b>Upload Audio Files</b></Label>
                            {/* <Input type="file" placeholder="Image" autoComplete="Image" accept="audio/*" onChange={(e) => this._handleAudioChange(e, index)} />
                            {val && val.audio.substr(41, 55) || ""} */}
                            <br></br>
                            <label for={"file-upload" + index * 2} class="custom-file-upload" style={_this.state.custom}  >
                              <i class="fa fa-cloud-upload"></i> Choose Audio
                            </label>
                            <br></br>
                            {val && val.audio && val.audio.substr(41, 55) || ""}
                            <Input id={"file-upload" + index * 2} type="file" accept="audio/*" style={_this.state.input} onChange={(e) => this._handleAudioChange(e, index)} />
                          </FormGroup>
                        </div>
                      )
                    }
                    {
                      (this.state.loader) ?
                        <Loader
                          type="TailSpin"
                          color="#00BFFF"
                          height={50}
                          width={50}
                          class="loader"
                          timeout={150000000}
                        />
                        : ""
                    }
                    <Button color="primary" class="btn btn-primary" disabled={(this.state.disable) ? true : false}>Submit</Button>

                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
        <ToastContainer />
      </div>
    )
  }
}

export default AddNewAudio;
