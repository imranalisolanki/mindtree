import React, { Component, Fragment } from 'react';
import ReactAudioPlayer from 'react-audio-player'
import jwt from 'jsonwebtoken'
import S3FileUpload from 'react-s3'
import { Link } from 'react-router-dom';
import { MultipleSelect } from "react-select-material-ui";
import '../../scss/vendors/bootstrap-pagination.css';
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import { Card, CardBody, CardHeader, Col, Badge } from 'reactstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Grid } from '@material-ui/core'
import { Button, TextField, MenuItem } from '@material-ui/core'
import { DialogContent, DialogContentText, Dialog, } from '@material-ui/core'
import FilterableTable from 'react-filterable-table'
import axios from 'axios'
import AppConfig from '../../config/appConfig'
import qs from 'qs'
import _ from 'lodash'
import moment from 'moment';
const decoded = jwt.decode(localStorage.token);
const getBadge = (status) => {
  return status === 'Active' ? 'success' :
    status === 'Inactive' ? 'secondary' :
      status === 'Pending' ? 'warning' :
        status === 'Banned' ? 'danger' :
          'primary'
}
const config = {
  bucketName: 'mindtree1',
  dirName: 'audio', /* optional */
  region: 'us-east-2',
  accessKeyId: AppConfig.accesskey,
  secretAccessKey: AppConfig.secretKey,
}
class Audios extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startAt: 0,
      currentPage: 1,
      activePage: 1,
      itemsPerPage: 10,
      limit: 10,
      skip: 0,
      scroll: 'paper',
      open: false,
      loader: false,
      load: false,
      loadr: false,
      loadMore: false,
      disable: true,
      image: [],
      trackImage: null,
      banner: null,
      track: null,
      type: '',
      name: '',
      name0: '',
      name1: '',
      name2: '',
      name3: '',
      name4: '',
      name5: '',
      day: '',
      inputs: [],
      categoryList: [],
      cat: "",
      err: null,
      audioList: [],
      audioAllList: [],
      lockError: '',
      nameError: '',
      positionError: '',
      typeError: '',
      bannerError: '',
      sizeError: '',
      isLocked: null,
      position: null,
      type: null
    }
    // this.state = { inputs: ['input-0'] };
    this.handleOnChange = this.handleOnChange.bind(this)
    this.handleOnChangeName = this.handleOnChangeName.bind(this)
    this.handleOnChangeDay = this.handleOnChangeDay.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this._handleAudioChange = this._handleAudioChange.bind(this)
    this._handleBannerChange = this._handleBannerChange.bind(this)
    // this._handleBgImageChange = this._handleBgImageChange.bind(this)
  }
  componentDidMount() {
    const pageNumber = 1
    this.getAudioListData(pageNumber)
    this.getCategoryData()
    // this.setInputs({})
  }
  getCategoryData() {
    var cond = {
      filter: {
        order: ['created DESC']
      }
    }
    const query = qs.stringify(cond, { addQueryPrefix: true })
    axios.get(AppConfig.ApiUrl + `categories` + query, AppConfig.headerConfig).then(res => {
      if (res && res.data) {
        // let cat = (res && res.data) ? res.data : []
        let catName = []
        _.forEach(res && res.data, function (val) {
          catName.push(val.name)
        })
        this.setState({
          categoryList: catName
        })
      } else {
        this.setState({ categoryList: [] })
      }
    }).catch(err => {

      console.log(err)
    })
  }
  handleClose() {
    this.setState({
      open: false, err: null
    })
  }
  handleClickOpen(scrollType) {
    this.setState({
      open: true,
      disable: true,
      scroll: scrollType,
      name: '',
      banner: '',
      audio: [],
      cat: [],
      type: null,
      lockError: '',
      nameError: '',
      position: null,
      isLocked: null,
      positionError: '',
      typeError: '',
      bannerError: '',
      sizeError: '',
    })
  };
  getAudioListData(pageNumber) {
    const { itemsPerPage } = this.state
    const activePage = pageNumber
    // const sortOrder = event
    this.setState({ activePage })
    const startAt = pageNumber * itemsPerPage - itemsPerPage
    var cond = {
      filter: {
        include: [
          { relation: "user" }
        ],
        order: ['position ASC']
      }
    }
    const query = qs.stringify(cond, { addQueryPrefix: true })
    axios.get(AppConfig.ApiUrl + `audio` + query, AppConfig.headerConfig).then(res => {
      if (res && res.data) {
        const audioAllList = (res && res.data) ? res.data : []
        this.setState({
          audioAllList, startAt
        })
      } else {
        this.setState({
          audioAllList: []
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }

  _handleBannerChange(e) {
    this.setState({ load: true, disable: true })
    var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
    const file = "treeImage" + currentDate + e.target.files[0].name;

    var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
    var newFile = new File([filesObject], file, { type: e.target.files[0].type });
    var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
    var newFile = new File([filesObject], file, { type: e.target.files[0].type });

    S3FileUpload
      .uploadFile(newFile, config)
      .then(data => {

        this.setState({ banner: data.location, load: false, disable: false, bannerError: '' })
      })
      .catch(err => console.error(err))
  }
  // _handleBgImageChange(e) {
  //   this.setState({ loadr: true, disable: true })
  //   var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
  //   const file = "bgImage" + currentDate + e.target.files[0].name;
  //   var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
  //   var newFile = new File([filesObject], file, { type: e.target.files[0].type });
  //   S3FileUpload
  //     .uploadFile(newFile, config)
  //     .then(data => {
  //       this.setState({ trackImage: data.location, loadr: false, disable: false })
  //     })
  //     .catch(err => console.error(err))

  // }

  _handleAudioChange(e) {
    this.setState({ loader: true, disable: true, loadMore: true })
    var imageList = []
    var currentDate = moment().unix()
    const file = "audio" + currentDate + e.target.files[0].name;
    var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
    var newFile = new File([filesObject], file, { type: e.target.files[0].type });
    S3FileUpload
      .uploadFile(newFile, config)
      .then(data => {
        if (data && data.location) {
          this.setState({ loader: false, disable: false, loadMore: false, audioError: '' })
          if (this.state.name && this.state.name !== '' && this.state.name0 === '') {
            imageList.push({ title: this.state.name, audio: data.location, size: this.state.size })
            var list = []
            list.push({ title: this.state.name, audio: data.location, size: this.state.size })
            this.setState({
              audioList: list
            })
          } else {
            if (data && data.location && this.state.name == '') {
              this.state.audioList.push({ title: this.state.name0, audio: data.location, size: this.state.size0 })
            }
          }
          if (this.state.name0 && this.state.name0 !== '' && this.state.name1 === '') {
            this.state.audioList.push({ title: this.state.name0, audio: data.location, size: this.state.size0 })
          }

          if (this.state.name1 && this.state.name1 !== '' && this.state.name2 === '') {
            this.state.audioList.push({ title: this.state.name1, audio: data.location, size: this.state.size1 })
          }
          if (this.state.name2 && this.state.name2 !== '' && this.state.name3 === '') {
            this.state.audioList.push({ title: this.state.name2, audio: data.location, size: this.state.size2 })
          }
          if (this.state.name3 && this.state.name3 !== '' && this.state.name4 === '') {
            this.state.audioList.push({ title: this.state.name3, audio: data.location, size: this.state.size3 })
          }
          if (this.state.name4 && this.state.name4 !== '' && this.state.nam5 === '') {
            this.state.audioList.push({ title: this.state.name4, audio: data.location, size: this.state.size4 })
          }
          if (this.state.nam5 && this.state.nam5 !== '') {
            this.state.audioList.push({ title: this.state.nam5, audio: data.location, size: this.state.size5 })
          }
        } else {
          this.setState({ loader: false })
        }

      }).catch(err => console.error(err))
    // })
    const image = this.state.audioList
    this.setState({ image })
  }

  handleOnSubmit(event) {
    // event.preventDefault()
    let payload = {
      name: this.state.name,
      categoryId: this.state.cat && this.state.cat !== '' && this.state.cat || this.state.userData && this.state.userData.categoryId || [],
      type: (this.state.type != null) ? this.state.type : null,
      position: (this.state.position != null) ? Number(this.state.position) : null,
      isLocked: (this.state.isLocked != null) ? this.state.isLocked : null,
      bannerImage: (this.state.banner === null) ? this.state.userData && this.state.userData.bannerImage || "" : this.state.banner,
      audio: (this.state.audioList && this.state.audioList.length) ? this.state.audioList : this.state.userData && this.state.userData.audio || [],
      userId: decoded && decoded.id || "5ea01990bbb07a27a0b130e0",
      modiefy: moment().toISOString()
    }

    if (payload.isLocked == null) {
      this.setState({
        lockError: "Audio Locked is required"
      })
      return false
    } else if (payload.type == "" || payload.type == null) {
      this.setState({
        typeError: "Type is required"
      })
      return false
    }
    else if (isNaN(payload.position) || this.state.position == "" || payload.position == null) {
      this.setState({
        positionError: "Position is required"
      })
      return false
    } else if (payload.bannerImage == "") {
      this.setState({
        bannerError: "Banner Image is required"
      })
      return false
    }
    else if (payload.name == "") {
      this.setState({
        nameError: "Title is required"
      })
      return false
    }
    else if (this.state.size == "" || this.state.size == undefined) {
      this.setState({
        sizeError: "Audio Duration is required"
      })
      return false
    }
    else if (payload && payload.audio && payload.audio.length <= 0) {
      this.setState({
        audioError: "Audio is required"
      })
      return false
    }

    const { match } = this.props
    if (match && match.params && match.params.id) {
      axios.patch(AppConfig.ApiUrl + `audio/${match.params.id}`, payload).then(res => {
        if (res) {
          toast.success("Updated sucessfully!")
          this.setState({ err: null })
          setTimeout(() => {
            this.getAudioListData(1)
            this.handleClose()
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
        this.setState({
          err: "All fields are mandatory"
        })
      })
    } else {
      axios.post(AppConfig.ApiUrl + `audio`, payload).then(res => {
        if (res && res.data) {
          toast.success("Audio Added sucessfully!")
          this.setState({
            err: null,
            name: '',
            banner: '',
            audio: [],
            audioList: [],
            cat: [],
            lockError: '',
            isLocked: null,
            nameError: '',
            positionError: '',
            typeError: '',
            bannerError: '',
            sizeError: '',
            audioError: '',
            type: null,
            position: null
          })
          setTimeout(() => {
            this.getAudioListData(1)
            this.handleClose()
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
        this.setState({
          err: "All fields are mandatory"
        })
      })
    }
  }
  handleOnChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
      disable: false
    })
    if (e.target.name == 'isLocked') {
      this.setState({ lockError: '' })
    } else if (e.target.name == 'type') {
      this.setState({ typeError: '' })
    } else if (e.target.name == 'name' && e.target.value != "") {
      if (this.state.audioList && this.state.audioList.length) {
        this.state.audioList[0].title = e.target.value
      }
      this.setState({ nameError: '' })
    } else if (e.target.name == 'position' && e.target.value != "") {
      this.setState({ positionError: '' })
    }
    else if (e.target.name == 'size' && e.target.value != "") {
      if (this.state.audioList && this.state.audioList.length) {
        this.state.audioList[0].size = e.target.value
      }
      this.setState({ sizeError: '' })
    }
  }
  handleOnChangeName(event) {
    this.setState({
      name: event.target.value
    })
  }
  handleOnChangeDay(event) {
    this.setState({
      day: event.target.value
    })
  }
  handleChange(e) {
    if (e && e.length) {
      e.concat(e)
      console.log(e)
      this.setState({
        cat: e
      })
    } else {
      this.setState({
        cat: ''
      })
    }
  }
  deleteAudioFile(id) {
    let cnd = window.confirm("Do you want delete audio?")
    if (cnd) {

      axios.delete(AppConfig.ApiUrl + `audio/${id}`, AppConfig.headerConfig).then(res => {
        if (res) {
          toast.success("Deleted sucessfully!")
          setTimeout(() => {
            this.getAudioListData()
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
      })
    } else {
      console.log('else part is here')
    }
  }

  appendInput() {
    var newInput = `input-${this.state.inputs.length}`;
    this.setState(prevState => ({ inputs: prevState.inputs.concat([newInput]) }));
  }

  checkAndRemovePositionLibrary() {
    let payload = {
      filter: {
        where: {
          position: this.state.position
        }
      }
    }
    payload = qs.stringify(payload, { addQueryPrefix: true })
    axios.get(AppConfig.ApiUrl + `audio/${payload}`, AppConfig.headerConfig).then(res => {
      if (res && res.status == 200) {
        if (res && res.data.length) {
          let audioId = res && res.data.length && res.data[0] && res.data[0].id || "null"
          axios.delete(AppConfig.ApiUrl + `audio/${audioId}`, AppConfig.headerConfig).then(ress => {
            if (ress && ress.status == 204) {
              console.log(ress.status, '=======')
            }
          }).catch(err => {
            console.log(err, '===-=-er=====')
          })
        }
      }
    }).catch(err => {
      console.log(err, '=============-=-=-')
    })
  }
  render() {

    let _this = this
    var data = [];
    var pgNo = this.state.startAt;
    _.forEach(this.state.audioAllList, function (val, index) {

      data.push({
        // id: pgNo+index+1,
        name: val && val.audio && val.audio[0] && val.audio[0].title || "",
        type: (val && val.type) ? val.type : "",
        banner: <img src={val.bannerImage} style={{ height: '50px', width: '25%' }} alt="ing"></img>,
        position: (val && val.position) ? val.position : 0,
        file: <ReactAudioPlayer
          src={(val && val.audioFiles && val.audioFiles.length) ? val.audioFiles : val.audio && val.audio[0] && val.audio[0].audio}
          controls
        />,
        action: <div><Link to={`/Library/editAudio/${val.id}`}><Badge color={getBadge('View')}>Edit</Badge></Link>&nbsp;&nbsp;<span onClick={() => { _this.deleteAudioFile(val.id) }} style={{ cursor: "pointer" }}><Badge color={getBadge('Active')}>Delete</Badge></span></div>
      })
    })
    const fields = [

      { name: 'name', displayName: "Title", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'type', displayName: "File Type", inputFilterable: true, sortable: true },
      { name: 'banner', displayName: "Banner Image", inputFilterable: false, exactFilterable: false, sortable: false, class: "test" },
      { name: 'position', displayName: "Position", inputFilterable: true, sortable: true },
      { name: 'file', displayName: "Audio File", inputFilterable: false, exactFilterable: false, sortable: false, class: "test" },
      { name: 'action', displayName: "Action", inputFilterable: false, sortable: false },
    ];
    return (
      <Fragment >
        <Col xl={12}>

          <Card>
            <CardHeader>
              <i className="fa fa-align-justify"></i> Library <small className="text-muted">List</small>
              <button style={{ float: 'right' }} class="btn btn-primary" onClick={() => { this.handleClickOpen('paper') }}> Add New</button>
            </CardHeader>
            <CardBody>
              <FilterableTable
                namespace="People"
                initialSort="modify"
                topPagerVisible={false}
                data={data}
                fields={fields}
                noRecordsMessage="There are no Audio to display"
                noFilteredRecordsMessage="Audio not found!"
              />
            </CardBody>
          </Card>

        </Col>
        <Dialog
          open={this.state.open}
          // onClose={handleClose}
          scroll={this.state.scroll}
          className="modalWrapper"
        >

          <CardHeader>
            {/*  <Grid className="modalTitle"> */}
            <h3>Add New Audio Files</h3>
            {/* </Grid> */}
          </CardHeader>

          <DialogContent dividers={this.state.scroll === 'paper'}>
            <DialogContentText>
              <p style={{ color: 'red', textAlign: "center" }}>{this.state.err !== null && this.state.err}</p>
              <Grid container spacing={3}>


                <div style={{ width: '98%', marginLeft: '1.5%', marginRight: '1.5%' }}>
                  <Grid item sm={12} xs={12}>
                    <MultipleSelect
                      label="Choose Category"
                      values={this.state.cat}
                      options={this.state.categoryList}
                      onChange={this.handleChange}
                    />
                  </Grid>
                  <Grid item sm={12} xs={12}>
                    <TextField
                      fullWidth
                      select
                      required
                      label="Locked Audio"
                      className='inputField'
                      onChange={this.handleOnChange}
                      name="isLocked"
                      error={this.state.lockError.length === 0 ? false : true}
                      helperText={this.state.lockError}
                    // onChange={handleAddInput}
                    >
                      <MenuItem key='yes' value={true}>Yes</MenuItem>
                      <MenuItem key='no' value={false}>No</MenuItem>
                    </TextField>
                  </Grid></div>


                <Grid item sm={12} xs={12}>
                  <TextField
                    fullWidth
                    select
                    required
                    label="File Type"
                    className='inputField'
                    onChange={this.handleOnChange}
                    name="type"
                    error={this.state.typeError.length === 0 ? false : true}
                    helperText={this.state.typeError}
                  // onChange={handleAddInput}
                  >
                    <MenuItem key='single' value="single">Single </MenuItem>
                    <MenuItem key='multiple' value="multiple">Multiple</MenuItem>
                    <MenuItem key='courses' value="courses">Courses</MenuItem>
                  </TextField>
                </Grid>

                <Grid item sm={12} xs={12}>
                  <TextField
                    type="number"
                    fullWidth
                    required
                    label="Audio Position"
                    name="position"
                    className='inputField'
                    onChange={this.handleOnChange}
                    error={this.state.positionError.length === 0 ? false : true}
                    helperText={this.state.positionError}
                    onBlur={(e) => { this.checkAndRemovePositionLibrary(e) }}
                  />
                </Grid>

                <Grid item sm={12} xs={12}>
                  <label>Upload Banner Image</label>
                  {
                    (this.state.load) ?
                      <Loader
                        type="TailSpin"
                        color="#00BFFF"
                        height={50}
                        width={50}
                        class="loader"
                        timeout={150000000}
                      />
                      : ""
                  }
                  <input type="file" placeholder="Image" autoComplete="Image" accept="image/*" onChange={(e) => this._handleBannerChange(e)} required />
                  <p style={{ color: 'red', fontSize: '0.75rem', marginTop: '3px' }}>{this.state.bannerError}</p>
                </Grid>

                <Grid item sm={12} xs={12}>
                  <TextField
                    fullWidth
                    required
                    label="Track Title"
                    name="name"
                    className='inputField'
                    error={this.state.nameError.length === 0 ? false : true}
                    helperText={this.state.nameError}
                    onChange={this.handleOnChange}
                  />
                </Grid>
                <Grid item sm={12} xs={12}>
                  <TextField
                    fullWidth
                    required
                    label="Audio Duration"
                    className='inputField'
                    onChange={this.handleOnChange}
                    name="size"
                    error={this.state.sizeError.length === 0 ? false : true}
                    helperText={this.state.sizeError}
                  // onChange={handleAddInput}
                  >
                  </TextField>
                </Grid>
                <Grid item sm={6} xs={12}>
                  <label>Upload Audio File</label>

                  <input type="file" placeholder="Image" autoComplete="Image" accept="audio/*" onChange={(e) => this._handleAudioChange(e)} required />
                  <p style={{ color: 'red', fontSize: '0.75rem', marginTop: '3px' }}>{this.state.audioError}</p>
                </Grid>
                <Grid item sm={6} xs={12}></Grid>

                {
                  (this.state.type === "multiple" || this.state.type === "courses") ?
                    this.state.inputs && this.state.inputs.length > 0 && this.state.inputs.map((input, index) =>
                      <div style={{ width: '100%', marginLeft: "11px", paddingTop: '25px' }}><Grid item sm={12} xs={12}>
                        <TextField
                          fullWidth
                          required
                          label="Track Title"
                          name={"name" + index}
                          key={input}
                          className='inputField'
                          onChange={this.handleOnChange}
                        />
                      </Grid>
                        <Grid item sm={12} xs={12}>
                          <TextField
                            fullWidth
                            required
                            label="Audio Duration"
                            className='inputField'
                            onChange={this.handleOnChange}
                            name={"size" + index}
                          // onChange={handleOnChange}
                          >
                          </TextField>
                        </Grid>
                        <Grid item sm={6} xs={12} style={{ paddingTop: '25px' }}></Grid>
                        <Grid item sm={6} xs={12}>
                          <label>Upload Audio File</label>

                          <input type="file" key={input} placeholder="Image" autoComplete="Image" accept="audio/*" onChange={(e) => this._handleAudioChange(e)} />
                        </Grid>

                      </div>
                    )
                    : ""
                }
                {
                  (this.state.type === "multiple" || this.state.type === "courses") ?
                    <div style={{ paddingTop: '25px', width: "100%" }}>
                      <Grid item sm={6} xs={12}></Grid>
                      <Grid item sm={6} xs={12} style={{ textAlign: "right", cursor: "pointerr", float: "right" }}>
                        <span className="btn bg-default" onClick={() => this.appendInput()} style={{ textAlign: "right", cursor: "pointerr" }}> Add More </span>
                      </Grid>
                    </div>
                    : ""
                }

                {/* {
                  
                    this.state.inputs && this.state.inputs.length this.state.inputs.map((input) => 
                       <Grid item sm={12} xs={12}>
                        <TextField
                          fullWidth
                          label="Track Title"
                          name="name"
                          key={input}
                          className='inputField'
                          onChange={this.handleOnChangeName}
                        />
                        </Grid>
                      <Grid item sm={6} xs={12}>
                        <label>Upload Audio File</label>
                        <input type="file" placeholder="Image" autoComplete="Image" accept="audio/*" onChange={(e) => this._handleAudioChange(e)} />
                      </Grid>
                      )
                    } */}

              </Grid>


            </DialogContentText>
          </DialogContent>
          <Grid className="modalFooter">
            {
              (this.state.loadMore) ?
                <Loader
                  type="TailSpin"
                  color="#00BFFF"
                  height={50}
                  width={50}
                  className="loader"
                  timeout={150000000}
                />
                : ""
            }
            <Button className="bg-warning" onClick={() => { this.handleClose() }}> Close </Button>
            <Button className="btn bg-default" onClick={() => { this.handleOnSubmit() }} disabled={(this.state.disable) ? true : false}> Save </Button>

          </Grid>
        </Dialog>
        <ToastContainer />
      </Fragment>

    )
  }
}

export default Audios;
