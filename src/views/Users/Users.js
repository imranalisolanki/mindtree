import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../scss/vendors/bootstrap-pagination.css';
import { Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
import FilterableTable from 'react-filterable-table'
import axios from 'axios'
import AppConfig from '../../config/appConfig'
// import qs from 'qs'
import _ from 'lodash'
// import moment from 'moment'
// const getBadge = (status) => {
//   return status === 'Active' ? 'success' :
//     status === 'Inactive' ? 'secondary' :
//       status === 'Pending' ? 'warning' :
//         status === 'Banned' ? 'danger' :
//           'primary'
// }
class Users extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startAt: 0,
      currentPage: 1,
      activePage: 1,
      itemsPerPage: 10,
      limit: 10,
      skip: 0,
      audioList: []
    }
  }
  componentDidMount() {
    const pageNumber = 1
    this.getUsersListData(pageNumber)
  }

  getUsersListData(pageNumber) {
    const { itemsPerPage } = this.state
    const activePage = pageNumber
    // const sortOrder = event
    this.setState({ activePage })
    const startAt = pageNumber * itemsPerPage - itemsPerPage
    // var cond = {
    //   filter: {
    //     order: ['created DESC']
    //   }
    // }
    // const query = qs.stringify(cond, { addQueryPrefix: true })
    axios.get(AppConfig.ApiUrl + `users/getUserList`, AppConfig.headerConfig).then(res => {
      if (res && res.data) {
        const audioList = (res && res.data) ? res.data : []
        this.setState({
          audioList, startAt
        })
      } else {
        this.setState({
          audioList: []
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }

  render() {
    var data = [];
    // var pgNo = this.state.startAt;
    _.forEach(this.state.audioList, function (val, index) {
      // console.log(val.lastAccess.substr(0, 10),'===========-=-=-=-')
      // let date = moment(val.lastAccess).format('DD-MM-YYYY') 
      data.push({
        // Id: pgNo + index + 1,

        name: <Link to={`/Users/UserDetail/${val.id}`}>{val.name}</Link>,
        email: (val && val.email) ? val.email : "",
        subscription: (val && val.subscription && val.subscription.id) ? "Yes" : "No",
        lastAccess: val.lastAccess.substr(0, 10),
        // day: (val && val.day) ? val.day : 1,
        treePlanted: (val && val.treePlanted) ? val.treePlanted : 0,
        co2Reduced: (val && val.co2Reduced) ? val.co2Reduced : 0,
      })
    })

    const fields = [
      // { name: 'Id', displayName: "Id", inputFilterable: true, sortable: true },
      { name: 'name', displayName: "Name", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'email', displayName: "Email", inputFilterable: true, exactFilterable: false, sortable: true },
      { name: 'subscription', displayName: "Subscription", inputFilterable: false, exactFilterable: false, sortable: true },
      { name: 'lastAccess', displayName: "Last Access", inputFilterable: true, exactFilterable: false, sortable: true },
      // { name: 'day', displayName: "Day streak", inputFilterable: true, sortable: true },
      { name: 'treePlanted', displayName: "Tree Planted", inputFilterable: true, sortable: true },
      { name: 'co2Reduced', displayName: "Co2 Emission", inputFilterable: false, sortable: true },

    ];
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Users <small className="text-muted">List</small>
                <Link to={`/Users/AddNewUser`} style={{ float: 'right' }}> <button class="btn btn-primary" style={{ float: "right" }}> Add New</button></Link>
              </CardHeader>
              <CardBody>
                <FilterableTable
                  namespace="Users"
                  initialSort="created"
                  topPagerVisible={false}
                  data={data}
                  fields={fields}
                  noRecordsMessage="There are no Users to display"
                  noFilteredRecordsMessage="Users not found!"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Users;
