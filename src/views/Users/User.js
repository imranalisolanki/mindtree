import React, { Component } from 'react';
import { Card, CardHeader, CardBody, Col, Row, Table, Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import '../../scss/vendors/bootstrap-pagination.css';
import FilterableTable from 'react-filterable-table'
import axios from 'axios'
import '../Users/user.css'
import config from '../../config/appConfig'
import moment from 'moment'
import _ from 'lodash'
import qs from 'qs'
import icon from '../../assets/img/icon.png'
const getBadge = (status) => {
  return status === 'Active' ? 'success' :
    status === 'Inactive' ? 'secondary' :
      status === 'Pending' ? 'warning' :
        status === 'Banned' ? 'danger' :
          'primary'
}

const jobData = [
  {
    days: "8",
    session: "2.30 Min",
    date: "30 June",
    treePlanted: 0,
    dayStreak: "01"
  },
  {
    days: "8",
    session: "0.30 Min",
    date: "30 June",
    treePlanted: 0,
    dayStreak: "01"
  },
  {
    days: "7",
    session: "2.30 Min",
    date: "29 June",
    treePlanted: 1,
    dayStreak: "07"
  },
  {
    days: "6",
    session: "2.30 Min",
    date: "28 June",
    treePlanted: 0,
    dayStreak: "06"
  },
  {
    days: "5",
    session: "2.30 Min",
    date: "27 June",
    treePlanted: 0,
    dayStreak: "05"
  },
  {
    days: "4",
    session: "2.30 Min",
    date: "26 June",
    treePlanted: 0,
    dayStreak: "04"
  },
  {
    days: "3",
    session: "2.30 Min",
    date: "25 June",
    treePlanted: 0,
    dayStreak: "03"
  },
  {
    days: "2",
    session: "2.30 Min",
    date: "24 June",
    treePlanted: 0,
    dayStreak: "02"
  },
  {
    days: "1",
    session: "1.30 Min",
    date: "23 June",
    treePlanted: 0,
    dayStreak: "01"
  }
]
const invitation = [
  {
    date: "30 June",
    name: "shekhar",
    email: "shekhar@yahoo.in",
    tree: 1
  },
  {
    date: "28 June",
    name: "gaurav",
    email: "gaurav@yahoo.in",
    tree: 0
  },
  {
    date: "26 June",
    name: "manish",
    email: "manish@yahoo.in",
    tree: 0
  },
  {
    date: "24 June",
    name: "akash",
    email: "akash@yahoo.in",
    tree: 1
  }
]
class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: [],
      jobData: [],
      activeTab: new Array(4).fill('1'),
      session: [],
      invitation: [],
      totalTrees: 0
    }

  }


  componentDidMount() {
    if (this.props && this.props.match && this.props.match.params && this.props.match.params.id) {
      this.getMemberDetailsById(this.props.match.params.id)
      this.getUserSessions();
      this.getInvitedUsers(this.props.match.params.id);
    }
  }
  

  getUserSessions() {
    let query = {
      filter: {
        where: {
          userId: this.props.match.params.id
        }
      }
    }
    query = qs.stringify(query, { addQueryPrefix: true })
    axios.get(config.ApiUrl + `sessions` + query, config.headerConfig).then(res => {
      if (res && res.data) {
        const totalTree = _.sumBy(res.data, v => v.tree)
        this.setState({
          session: res && res.data || [], totalTrees: totalTree
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }

  getMemberDetailsById(id) {
    axios.get(config.ApiUrl + `users/` + id, config.headerConfig).then(res => {
      if (res) {
        const userData = res.data
        this.setState({ userData })
      }
    }).catch(err => {
      console.log(err)
    })
  }
  getInvitedUsers(userId) {
    axios.get(config.ApiUrl + `users/getInvitedUsers/` + userId, config.headerConfig).then(res => {
      if (res && res.data) {
        const invitation = res.data
        this.setState({ invitation })
      }
    }).catch(err => {
      console.log(err)
    })
  }

  toggle(tabPane, tab) {
    const newArray = this.state.activeTab.slice()
    newArray[tabPane] = tab
    this.setState({ activeTab: newArray, });
  }

  render() {
    let _this = this
    var data = [];
    var invite = [];
    
    _.forEach(this.state.session, function (val, index) {
      data.push({
        day: val.dayStreak,
        date: moment(val.created).format('DD MMM YYYY'),
        session: (val && val.sessionTime) ? val.sessionTime + ' Mints' : '00:00 Mints',
        dayStreak: val.dayStreak,
        treePlanted: val.tree
      })
    })

    const fields = [
      { name: 'day', displayName: "No of days ", inputFilterable: true, sortable: true },
      { name: 'date', displayName: "Date ", inputFilterable: true, sortable: true },
      { name: 'session', displayName: "Session Time", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'dayStreak', displayName: "Days Streak", inputFilterable: false, exactFilterable: false, sortable: true },
      { name: 'treePlanted', displayName: "Tree", inputFilterable: false, exactFilterable: false, sortable: true },

    ];
    _.forEach(this.state.invitation, function (val, index) {
      invite.push({
        date: moment(val.created).format('DD MMM YYYY'),
        name: val.name,
        email: val.email,
        tree: val.tree
      })
    })

    const field = [
      { name: 'date', displayName: "Date ", inputFilterable: true, sortable: true },
      { name: 'name', displayName: "Name", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'email', displayName: "Email", inputFilterable: false, exactFilterable: false, sortable: true },
      { name: 'tree', displayName: "Tree", inputFilterable: false, exactFilterable: false, sortable: true },

    ];
    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> User Details<small className="text-muted"></small>
                <span>
                </span>
              </CardHeader>
              <Col lg={12} style={{ margin: '14px 0px 0px 0px' }}>
                <Col lg={1} style={{ float: 'left' }}>
                  <img src={`${this.state.userData && this.state.userData.image !== "string" && this.state.userData.image || icon}`} alt="." style={{ width: '100%' }}></img>
                </Col>
                <Col lg={5} style={{ float: 'left' }}>
                  <ul style={{ listStyleType: "none" }}>
                    <li><b>Name</b>   :&nbsp;&nbsp;{this.state.userData.name} </li>
                    <li><b>Email</b>  :&nbsp;&nbsp; {this.state.userData.email}</li>
                    <li><b>Subscription</b>  :&nbsp;&nbsp;  Premium</li>
                  </ul>
                </Col>
                <Col lg={6} style={{ float: 'left', textAlign: "right" }}>
                  <ul style={{ listStyleType: "none" }}>
                    <li><b>Total Sessions</b> :&nbsp;&nbsp; {this.state.session && this.state.session.length || 0}</li>
                    <li><b>Tree Planted</b> :&nbsp;&nbsp; {this.state.totalTrees || 0} </li>
                    <li><b>Last Active</b> :&nbsp;&nbsp; {moment(this.state.userData.lastAccess).format('YYYY-MM-DD')} </li>
                  </ul>
                </Col>


              </Col>
              <CardBody>
                <Nav tabs>
                  <NavItem>
                    <NavLink
                      active={this.state.activeTab[0] === '1'}
                      onClick={() => { this.toggle(0, '1'); }}
                    >
                      Session
                </NavLink>
                  </NavItem>
                  <NavItem>
                    <NavLink
                      active={this.state.activeTab[0] === '2'}
                      onClick={() => { this.toggle(0, '2'); }}
                    >
                      Invitations
                </NavLink>
                  </NavItem>
                </Nav>
                <TabContent activeTab={this.state.activeTab[0]}>
                  <TabPane tabId="1">
                    <FilterableTable
                      namespace="session"
                      initialSort="created"
                      topPagerVisible={false}
                      data={data}
                      fields={fields}
                      noRecordsMessage="There are no sessions to display"
                      noFilteredRecordsMessage="sessions not found!"
                    />
                  </TabPane>
                  <TabPane tabId="2">
                    <FilterableTable
                      namespace="invite"
                      initialSort="created"
                      topPagerVisible={false}
                      data={invite}
                      fields={field}
                      noRecordsMessage="There are no invites to display"
                      noFilteredRecordsMessage="invites not found!"
                    />
                  </TabPane>
                </TabContent>

              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default User;
