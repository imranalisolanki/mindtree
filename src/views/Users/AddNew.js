import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, FormGroup, Input, Label, Button, Form } from 'reactstrap';
// import * as _ from 'lodash'
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { hashHistory } from 'react-router'
import axios from 'axios'
import AppConfig from '../../config/appConfig'

class AddNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      planList: [],
      email: '',
      message: ""

    }
    this.submit = this.submit.bind(this)
    this.handleOnChangeName = this.handleOnChangeName.bind(this)
  }
  componentDidMount() {
    this.getPlanList()
  }

  handleOnChangeName(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  getPlanList() {
    axios.get(AppConfig.ApiUrl + `plans`).then(res => {
      if (res && res.data) {
        this.setState({
          planList: (res && res.data) ? res.data : []
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }

  submit(e) {
    e.preventDefault()
    // let email = this.state.email.split(",")

    let payload = {
      email: this.state.email.split(","),
      msg: this.state.message,
      link: "",
    }

    axios.post(AppConfig.ApiUrl + `users/sendUserInvitation`, payload, AppConfig.headerConfig).then(res => {
      if (res) {
        toast.success("Invitation sent sucessfully!")
        setTimeout(() => {
          hashHistory.push('/Users')
        }, 1000);
      }
    }).catch(err => {
      console.log(err)
    })
  }

  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={12} >
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i>  Add New User <small className="text-muted"></small>
              </CardHeader>
              <Col xl={6} sm={{ offset: 3 }}>
                <Form autocomplete="off" onSubmit={this.submit} enctype='multipart/form-data'>
                  <CardBody>


                    <FormGroup>
                      <Input type="text" id="location" name="email" required placeholder="Add Email'ID Separated by comma" style={{ backgroundColor: '#ccc' }} onChange={this.handleOnChangeName} />
                    </FormGroup>
                    <FormGroup>
                      <Input type="textarea" name="message" id="textarea-input" rows="9"
                        placeholder="Add Specific Msg for user to download app" required onChange={this.handleOnChangeName} />
                    </FormGroup>
                    <FormGroup>
                      <Label htmlFor="company">Select Trial Pack </Label><br></br>
                      {
                        this.state.planList && this.state.planList.length && this.state.planList.map((plan, index) =>
                          <FormGroup>
                            <Input type="radio" id="location" placeholder="Status" name="names" id="name" style={{ marginLeft: '1px' }} /> <p style={{ marginLeft: '20px' }}>  {plan.name} -  {plan.type}     <span style={{ marginLeft: '10px' }}>(Valid upto {plan.planType})</span></p>
                          </FormGroup>
                        )
                      }
                    </FormGroup><br></br>
                    <Button type="submit" class="btn btn-primary" color="primary">Send Invitation</Button>
                  </CardBody>
                </Form>
              </Col>
            </Card>
          </Col>
        </Row>
        <ToastContainer />
      </div>
    );
  }
}

export default AddNew;
