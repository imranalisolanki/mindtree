import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import axios from 'axios'
import { hashHistory } from 'react-router';
import configApi from '../../../config/appConfig'
import logo from '../../../assets/img/logo1.png'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      error: null
    }
  }
  submit = (event) => {
    event.preventDefault()
    var loginDetails = {
      email: event.target[0].value,
      password: event.target[1].value
    }
    axios.post(configApi.ApiUrl + `admins/login`, loginDetails).then(res => {
      if (res && res.data) {
        localStorage.token = (res && res.data && res.data.token) ? res.data.token : ""
        setTimeout(() => {
          hashHistory.push('/dashboard');
          window.location.reload();
        }, 1000);
      }
    }).catch(errr => {
      let error = "Invalid email and password"
      this.setState({
        error
      })
    })
  }
  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6" className="image">
              <img src={logo} alt="mind" style={{ paddingBottom: '25px', width: '70%', marginLeft: '75px' }}></img>

              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form autoComplete="off" onSubmit={this.submit}>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder="Email" autoComplete="Email" required />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" autoComplete="current-password" required />
                      </InputGroup>
                      <p style={{ color: 'red' }}>{(this.state.error && this.state.error != null) ? this.state.error : ''}</p>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">Login</Button>
                        </Col>
                        {/* <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col> */}
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
