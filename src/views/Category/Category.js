import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import S3FileUpload from 'react-s3'
import '../../scss/vendors/bootstrap-pagination.css';
import {Card, CardBody, CardHeader, Col, Row, Badge, Form } from 'reactstrap';
// import { toast, ToastContainer } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';
import { Grid } from '@material-ui/core'
import { Button, TextField, MenuItem, FilledInput } from '@material-ui/core'
import Dropzone from 'react-dropzone-uploader';
import { DialogContent, DialogContentText, Dialog, } from '@material-ui/core'
import FilterableTable from 'react-filterable-table'
import axios from 'axios'
import AppConfig from '../../config/appConfig'
import qs from 'qs'
import _ from 'lodash'

const getBadge = (status) => {
  return status === 'Active' ? 'success' :
    status === 'Inactive' ? 'secondary' :
      status === 'Pending' ? 'warning' :
        status === 'Banned' ? 'danger' :
          'primary'
}
const config = {
  bucketName: 'mindtree1',
  dirName: 'category', /* optional */
  region: 'us-east-2',
  accessKeyId: AppConfig.accesskey,
  secretAccessKey: AppConfig.secretKey,
}
class Category extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startAt: 0,
      currentPage: 1,
      scroll: 'paper',
      open: false,
      activePage: 1,
      itemsPerPage: 10,
      limit:10,
      skip:0,
      icon:'',
      categoryList:[],
      name: "",
      price: 0,
      planType: "",
      type: "",
      employees: "",
      er:null,
      editId:'',
      editData:{}
    }
    this.handleOnChangeName = this.handleOnChangeName.bind(this)
    this._handleImageChange = this._handleImageChange.bind(this)
  }
  componentDidMount() {
    const pageNumber = 1
    this.getCategoryData(pageNumber)
  }
  handleOnChangeName(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  handleClickOpen(scrollType,id) {
    if(id && id !=undefined){
     let response =  _.filter(this.state.categoryList, function (val) {
        return val.id === id
      })
     this.setState({
       editId: id,
       editData: response && response[0] || {}
     })
    }
    this.setState({
      open: true,
      scroll: scrollType
    })
  };
  handleClose() {
    this.setState({
      open: false,
    })
    
  }
  _handleImageChange = (e) => {
    
    var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
    const file = "userImage" + currentDate + e.target.files[0].name;
    var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
    var newFile = new File([filesObject], file, { type: e.target.files[0].type });
    S3FileUpload
      .uploadFile(newFile, config)
      .then(data => {
        const icon = data.location
        this.setState({ icon })
      })
      .catch(err => console.error(err))
  }
  getCategoryData() {
    var cond = {
      filter: {
        order:['created DESC']
      }
    }
    const query = qs.stringify(cond, { addQueryPrefix: true })
    axios.get(AppConfig.ApiUrl + `categories` + query, AppConfig.headerConfig).then(res => {
      if(res && res.data){
        const categoryList = (res && res.data) ? res.data : []
        this.setState({
          categoryList
        })
      }else {
        this.setState({
          categoryList:[]
        }) 
      }
    }).catch(err =>{
     
      console.log(err)
    })
  }
  handleOnSubmit(event) {
   
    let plan = {
      name: this.state.name !== "" && this.state.name || this.state.editData && this.state.editData.name ,
      icon: (this.state.icon === '') ? this.state.editData && this.state.editData.icon : this.state.icon,
    
    }

    if (this.state.editId && this.state.editId !==undefined){
      axios.patch(AppConfig.ApiUrl + `categories/` + this.state.editId, plan, AppConfig.headerConfig).then(res => {
        if (res) {
         
          setTimeout(() => {
            this.getCategoryData(1)
            this.handleClose()
          }, 1000);
        }
      }).catch(err => {
        this.setState({
          er: "All Fields Are Mandatory"
        })
        console.log(err)
      })
    } else{
      axios.post(AppConfig.ApiUrl + `categories`, plan, AppConfig.headerConfig).then(res => {
        if (res) {
         
          setTimeout(() => {
            this.getCategoryData(1)
            this.handleClose()
          }, 1000);
        }
      }).catch(err => {
        this.setState({
          er: "All Fields Are Mandatory"
        })
        console.log(err)
      })
    }
    
    
  }
  render() {
    let _this = this
    var data = [];
    _.forEach(this.state.categoryList, function (val, index) {
      data.push({
        name: val.name,
        Icon: <img src={val.icon} style={{ height: '50px', width: '15%' }} alt="ing"></img>,
        action: <span style={{ cursor: 'pointer' }} onClick={() => { _this.handleClickOpen('paper',val.id) }}><Badge color={getBadge('Active')}>Edit</Badge></span>
       
      })
    })
    const fields = [
      { name: 'name', displayName: "Name", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'Icon', displayName: "Icon", inputFilterable: false, exactFilterable: false, sortable: false,class:"test" },
      { name: 'action', displayName: "Action", inputFilterable: false, sortable: false },
     
    ];
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Category <small className="text-muted">List</small>
                <button class="btn btn-primary" style={{ float: "right" }} onClick={() => { this.handleClickOpen('paper') }}> Add New</button>
              </CardHeader>
              <CardBody>
                <FilterableTable
                  namespace="People"
                  initialSort="modify"
                  topPagerVisible={false}
                  data={data}
                  fields={fields}
                  noRecordsMessage="There are no Category to display"
                  noFilteredRecordsMessage="Category not found!"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Dialog
          open={this.state.open}
          // onClose={handleClose}
          scroll={this.state.scroll}
          className="modalWrapper"
        >
          <CardHeader>
            {/*  <Grid className="modalTitle"> */}
            <h3>Add New Category</h3>
            {/* </Grid> */}
          </CardHeader>
          <DialogContent dividers={this.state.scroll === 'paper'}>
            <DialogContentText>
              <Grid container spacing={3}>
    
                <Grid item sm={12} xs={12}>
                  <p style={{color:'red'}}>{this.state.er}</p>
                  <TextField
                    fullWidth
                    label="Name"
                    name="name"
                    required
                    key={this.state.editData && this.state.editData.name}
                    defaultValue={this.state.editData && this.state.editData.name}
                    className='inputField'
                    onChange={this.handleOnChangeName}
                  />
                </Grid>
                <Grid item sm={6} xs={12} className="fileUpload">
                  <label>Upload Icon</label>
                 <input type="file" name="icon" onChange={this._handleImageChange}></input>
                </Grid>
               
              </Grid>
            </DialogContentText>
          </DialogContent>
          <Grid className="modalFooter">
            <Button className="bg-warning" onClick={() => { this.handleClose() }}> Close </Button>
            <Button className="btn bg-default" onClick={() => { this.handleOnSubmit() }}> Save </Button>

          </Grid>
        </Dialog>
      </div>
    )
  }
}

export default Category;
