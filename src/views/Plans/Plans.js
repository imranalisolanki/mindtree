import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import '../../scss/vendors/bootstrap-pagination.css';
import { Card, CardBody, CardHeader, Col, Row, Badge } from 'reactstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Grid } from '@material-ui/core'
import { Button, TextField } from '@material-ui/core'
// import Dropzone from 'react-dropzone-uploader';
import { DialogContent, DialogContentText, Dialog, } from '@material-ui/core'
import FilterableTable from 'react-filterable-table'
import axios from 'axios'
import AppConfig from '../../config/appConfig'
import qs from 'qs'
import _ from 'lodash'
// import moment from 'moment';
const getBadge = (status) => {
  return status === 'Active' ? 'success' :
    status === 'Inactive' ? 'secondary' :
      status === 'Pending' ? 'warning' :
        status === 'Banned' ? 'danger' :
          'primary'
}
class Plans extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startAt: 0,
      currentPage: 1,
      scroll: 'paper',
      open: false,
      activePage: 1,
      itemsPerPage: 10,
      limit: 10,
      skip: 0,
      audioList: [],
      name: "",
      price: 0,
      planType: "",
      type: "",
      employees: "",
      er: null,
      editId: '',
      editData: {}
    }
    this.handleOnChangeName = this.handleOnChangeName.bind(this)
  }
  componentDidMount() {
    const pageNumber = 1
    this.getPackListData(pageNumber)
  }
  handleOnChangeName(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  handleClickOpen(scrollType, id) {
    if (id && id != undefined) {
      let response = _.filter(this.state.audioList, function (val) {
        return val.id === id
      })
      this.setState({
        editId: id,
        editData: response && response[0] || {}
      })
    } else {
      this.setState({
        editData: ""
      })
    }
    this.setState({
      open: true,
      scroll: scrollType
    })
  };
  handleClose() {
    this.setState({
      open: false,
    })

  }
  getPackListData(pageNumber) {
    const { itemsPerPage } = this.state
    const activePage = pageNumber
    // const sortOrder = event
    this.setState({ activePage })
    const startAt = pageNumber * itemsPerPage - itemsPerPage
    var cond = {
      filter: {
        order: ['created DESC']
      }
    }
    const query = qs.stringify(cond, { addQueryPrefix: true })
    axios.get(AppConfig.ApiUrl + `plans` + query, AppConfig.headerConfig).then(res => {
      if (res && res.data) {
        const audioList = (res && res.data) ? res.data : []
        this.setState({
          audioList, startAt
        })
      } else {
        this.setState({
          audioList: []
        })
      }
    }).catch(err => {

      console.log(err)
    })
  }
  handleOnSubmit(event) {

    let plan = {
      name: (this.state.name != "" && this.state.name) ? this.state.name : (this.state.editData && this.state.editData.name) ? this.state.editData.name : "",
      planType: (this.state.planType != "" && this.state.planType) ? this.state.planType : (this.state.editData && this.state.editData.planType) ? this.state.editData.planType : "",
      type: (this.state.type != "" && this.state.type) ? this.state.type : (this.state.editData && this.state.editData.type) ? this.state.editData.type : "",
      employees: (this.state.employees != "" && this.state.employees) ? this.state.employees : (this.state.editData && this.state.editData.employees) ? this.state.editData.employees : "",
      userId: "5ea01990bbb07a27a0b130e0"
    }
    if (this.state.editId && this.state.editId != undefined) {
      axios.patch(AppConfig.ApiUrl + `plans/` + this.state.editId, plan, AppConfig.headerConfig).then(res => {
        if (res) {
          toast.success("Trial Pack Updated sucessfully!")
          setTimeout(() => {
            this.getPackListData(1)
            this.handleClose()
          }, 1000);
        }
      }).catch(err => {
        this.setState({
          er: "All Fields Are Mandatory"
        })
        console.log(err)
      })
    } else {
      axios.post(AppConfig.ApiUrl + `plans`, plan, AppConfig.headerConfig).then(res => {
        if (res) {
          toast.success("Trial Pack Added sucessfully!")
          setTimeout(() => {
            this.getPackListData(1)
            this.handleClose()
          }, 1000);
        }
      }).catch(err => {
        this.setState({
          er: "All Fields Are Mandatory"
        })
        console.log(err)
      })
    }


  }
  render() {
    let _this = this
    var data = [];
    _.forEach(this.state.audioList, function (val, index) {
      data.push({
        Id: val.name,
        name: val.type,
        employees: val.employees,
        valid: val.planType,
        action: <span style={{ cursor: 'pointer' }} onClick={() => { _this.handleClickOpen('paper', val.id) }}><Badge color={getBadge('Active')}>Edit</Badge></span>

      })
    })
    const fields = [
      { name: 'Id', displayName: "Id", inputFilterable: true, sortable: true },
      { name: 'name', displayName: "Name", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'employees', displayName: "Employees", inputFilterable: false, exactFilterable: false, sortable: false, class: "test" },
      { name: 'valid', displayName: "Valid", inputFilterable: true, sortable: true },
      { name: 'action', displayName: "Action", inputFilterable: false, sortable: false },

    ];
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Trial Pack <small className="text-muted">List</small>
                <button class="btn btn-primary" style={{ float: "right" }} onClick={() => { this.handleClickOpen('paper') }}> Add New</button>
              </CardHeader>
              <CardBody>
                <FilterableTable
                  namespace="People"
                  initialSort="modify"
                  topPagerVisible={false}
                  data={data}
                  fields={fields}
                  noRecordsMessage="There are no Plans to display"
                  noFilteredRecordsMessage="Plans not found!"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Dialog
          open={this.state.open}
          // onClose={handleClose}
          scroll={this.state.scroll}
          className="modalWrapper"
        >
          <CardHeader>
            {/*  <Grid className="modalTitle"> */}
            <h3>Add New Trial Pack</h3>
            {/* </Grid> */}
          </CardHeader>
          <DialogContent dividers={this.state.scroll === 'paper'}>
            <DialogContentText>
              <Grid container spacing={3}>

                <Grid item sm={12} xs={12}>
                  <p style={{ color: 'red' }}>{this.state.er}</p>
                  <TextField
                    fullWidth
                    label="Pack Id"
                    name="name"
                    required
                    key={this.state.editData && this.state.editData.name}
                    defaultValue={this.state.editData && this.state.editData.name}
                    className='inputField'
                    onChange={this.handleOnChangeName}
                  />
                </Grid>
                <Grid item sm={12} xs={12}>
                  <TextField
                    fullWidth
                    label="Name"
                    name="type"
                    required
                    key={this.state.editData && this.state.editData.type}
                    defaultValue={this.state.editData && this.state.editData.type}
                    className='inputField'
                    onChange={this.handleOnChangeName}
                  />
                </Grid>
                <Grid item sm={12} xs={12}>
                  <TextField
                    fullWidth
                    label="Employees"
                    name="employees"
                    required
                    key={this.state.editData && this.state.editData.employees}
                    defaultValue={this.state.editData && this.state.editData.employees}
                    className='inputField'
                    onChange={this.handleOnChangeName}
                  />
                </Grid>
                <Grid item sm={12} xs={12}>
                  <TextField
                    fullWidth
                    label="Valid"
                    name="planType"
                    required
                    key={this.state.editData && this.state.editData.planType}
                    defaultValue={this.state.editData && this.state.editData.planType}
                    className='inputField'
                    onChange={this.handleOnChangeName}
                  />
                </Grid>
              </Grid>
            </DialogContentText>
          </DialogContent>
          <Grid className="modalFooter">
            <Button className="bg-warning" onClick={() => { this.handleClose() }}> Close </Button>
            <Button className="btn bg-default" onClick={() => { this.handleOnSubmit() }}> Save </Button>

          </Grid>
        </Dialog>
        <ToastContainer />
      </div>
    )
  }
}

export default Plans;
