import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { Card, CardBody, Container, Col, Row, Form, FormGroup, Input, Button } from 'reactstrap';
import jwt from 'jsonwebtoken'
import axios from 'axios'
import moment from 'moment'
import AppConfig from '../../config/appConfig'
import { hashHistory } from 'react-router'
const decoded = jwt.decode(localStorage.token);


class AddNewPlan extends Component {
  constructor(props) {
    super(props)
    this.state = {
      image: [],
      banner: null,
      type: '',
      userData: {}
    }
    this.submit = this.submit.bind(this)
  }

  componentDidMount() {

  }



  submit(event) {
    event.preventDefault()
    let payload = {
      name: event.target.name.value,
      day: Number(event.target.day.value),
      type: event.target.type.value,
      bannerImage: (this.state.banner === null) ? this.state.userData.bannerImage : this.state.banner,
      audioFiles: (this.state.image && this.state.image.length) ? this.state.image : this.state.userData.audioFiles,
      userId: decoded.id,
      modiefy: moment().toISOString()
    }
    const { match } = this.props
    if (match && match.params && match.params.id) {
      axios.patch(AppConfig.ApiUrl + `plans/${match.params.id}`, payload).then(res => {
        if (res) {
          toast.success("Updated sucessfully!")
          setTimeout(() => {
            hashHistory.push('/Audio')
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
      })
    } else {
      axios.post(AppConfig.ApiUrl + `plans`, payload).then(res => {
        if (res && res.data) {
          toast.success("Audio Added sucessfully!")
          setTimeout(() => {
            hashHistory.push('/Audio')
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
      })
    }
  }

  render() {
    return (
      <div className="app  align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col xl={12}>
              <Card className="mx-2">
                <CardBody className="p-4">
                  <Col xl={6} sm={{ offset: 3 }}>
                    <Form onSubmit={this.submit} encType="multipart/form-data">
                      <FormGroup className="mb-3">
                        <Input type="text" name="id" placeholder="Plan ID" autoComplete="id" required />
                      </FormGroup>
                      <FormGroup className="mb-3">
                        <Input type="text" name="name" placeholder="Name" autoComplete="name" required />
                      </FormGroup>
                      <FormGroup className="mb-3">
                        <Input type="number" name="price" placeholder="Price" autoComplete="price" required />
                      </FormGroup>
                      <FormGroup className="mb-3">
                        <Input type="text" name="employee" placeholder="Employees" autoComplete="employee" required />
                      </FormGroup>
                      <FormGroup className="mb-3">
                        <Input type="text" name="valid" placeholder="Valid" autoComplete="valid" required />
                      </FormGroup>

                      <Button color="primary" class="btn btn-primary">Submit</Button>
                    </Form>
                  </Col>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default AddNewPlan;
