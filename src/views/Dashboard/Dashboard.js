import React, { Component, Fragment } from 'react';
import { Grid } from '@material-ui/core'
import axios from 'axios'
import _ from 'lodash'
import { Card, Col, Row, CardHeader } from 'reactstrap';
import Highcharts from 'highcharts';
import {
  HighchartsChart, Chart, withHighcharts, Tooltip, XAxis, YAxis, Legend, AreaSeries, SplineSeries
} from 'react-jsx-highcharts';
import appConfig from '../../config/appConfig';
import icon1 from '../../assets/img/download.png'
import icon2 from '../../assets/img/download.png'
import icon3 from '../../assets/img/download.png'
import icon4 from '../../assets/img/download.png'
require("highcharts/modules/annotations")(Highcharts);

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userCount: 0,
      treeplanted: 0,
      reduction: 0,
      minutes: 0,
      treeChart: [],
      reductionChart: []
    };
  }

  componentDidMount() {
    this.getUsersCount()
    this.getTreesCount()
    this.getco2ReductionCount()
    this.getTreePlantedChart()
    this.getco2ReductionChart()
  }
  getUsersCount() {
    axios.get(appConfig.ApiUrl + `users/count`, appConfig.headerConfig).then(res => {
      if (res && res.data) {
        this.setState({
          userCount: (res && res.data && res.data.count) ? res.data.count : 0
        })
      } else {
        this.setState({
          userCount: 0
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }

  getTreesCount() {
    axios.get(appConfig.ApiUrl + `tree-planteds`, appConfig.headerConfig).then(res => {
      if (res && res.data) {
        let planted = _.sumBy(res.data, v => v.planted)
        this.setState({
          treeplanted: planted
        })
      } else {
        this.setState({
          treeplanted: 0
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }
  getco2ReductionCount() {
    axios.get(appConfig.ApiUrl + `co2reductions`, appConfig.headerConfig).then(res => {
      if (res && res.data) {
        const reduction = _.sumBy(res.data, v => v.reduction)
        const minutes = _.sumBy(res.data, v => v.minutes)
        this.setState({
          reduction: reduction,
          minutes: minutes
        })
      } else {
        this.setState({
          reduction: 0,
          minutes: 0
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }

  getTreePlantedChart() {
    axios.get(appConfig.ApiUrl + `tree-planteds/treeChart`, appConfig.headerConfig).then(res => {
      if (res && res.data) {
        this.setState({
          treeChart: (res && res.data) ? res.data : []
        })
      } else {
        this.setState({
          treeChart: []
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }

  getco2ReductionChart() {
    axios.get(appConfig.ApiUrl + `co2reductions/coReductionChart`, appConfig.headerConfig).then(res => {
      if (res && res.data) {
        this.setState({
          reductionChart: (res && res.data) ? res.data : []
        })
      } else {
        this.setState({
          reductionChart: []
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {

    return (
      <div className="animated fadeIn">
        <Fragment>
          <Grid className="box5" item sm="12" container spacing={3} style={{ marginBottom: '10px' }}>
            <Grid item className="box box-1">
              <Grid className="analyticFeaturedWrap">
                <Grid className="analyticFeaturedContent">
                  <div className="box-inner">
                    <p>Total Users</p>
                    <h3>{this.state.userCount}</h3>
                  </div>
                  <img src={icon1} alt="icon" />
                </Grid>
              </Grid>
            </Grid>
            <Grid item className="box box-2">
              <Grid className="analyticFeaturedWrap">
                <Grid className="analyticFeaturedContent">
                  <div className="box-inner">
                    <p>Total Trees</p>
                    <h3>{this.state.treeplanted}</h3>
                  </div>
                  <img src={icon2} alt="icon" />
                </Grid>
              </Grid>
            </Grid>
            <Grid item className="box box-3">
              <Grid className="analyticFeaturedWrap">
                <Grid className="analyticFeaturedContent">
                  <div className="box-inner">

                    <p>Co2 Reduction</p>
                    <h3>{this.state.reduction}</h3>
                  </div>
                  <img src={icon3} alt="icon" />
                </Grid>
              </Grid>
            </Grid>
            <Grid item className="box box-4">
              <Grid className="analyticFeaturedWrap">
                <Grid className="analyticFeaturedContent">
                  <div className="box-inner">
                    <p>Total Minutes</p>
                    <h3>{this.state.minutes}</h3>
                  </div>
                  <img src={icon4} alt="icon" />
                </Grid>
              </Grid>
            </Grid>
            {/*  <Grid item className="box box-5">
              <Grid className="analyticFeaturedWrap">
                <Grid className="analyticFeaturedContent">
                  <div className="box-inner">
                    <p>total</p>
                    <h3>250</h3>
                  </div>
                  <img src={icon5} alt="icon" />
                </Grid>
              </Grid>
            </Grid> */}
            {/* <Grid item lg={4} sm={6} xs={12}>
                <Grid className="analyticFeaturedWrap">
                    <Grid className="analyticFeaturedContent">
                        <p>{leaderboard.name}</p>
                        <h3>{leaderboard.value}</h3>
                    </Grid>
                </Grid>
            </Grid> */}
          </Grid>
        </Fragment>
        <Row>
          <Col xs="12" sm="12" lg="6">

            <CardHeader>
              <p><b style={{ fontSize: '16px' }}>Tree Planted </b></p>
            </CardHeader>
            <Card className="text-white">
              <HighchartsChart>
                <Chart zoomType="x" />
                <Legend />
                <Tooltip valueSuffix="" />
                <XAxis id="myXaxis" type="datetime">
                </XAxis>
                <YAxis id="myYaxis">
                  <AreaSeries color="skyblue" name="Planted" data={this.state.treeChart} />
                </YAxis>
              </HighchartsChart>
            </Card>
          </Col>

          <Col xs="12" sm="12" lg="6">
            <CardHeader>
              <p><b style={{ fontSize: '16px' }}>Co2 Reduction </b></p>
            </CardHeader>
            <Card className="text-white">
              <HighchartsChart>
                <Chart zoomType="x" />
                <Legend />
                <Tooltip valueSuffix="" />
                <XAxis id="myXaxis" type="datetime">
                </XAxis>
                <YAxis id="myYaxis">
                  <SplineSeries color="#adc421" name="Co2Reduction" data={this.state.reductionChart} />
                </YAxis>
              </HighchartsChart>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withHighcharts(Dashboard, Highcharts);
