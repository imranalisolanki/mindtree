import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { MultipleSelect } from "react-select-material-ui";
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import { Card, CardBody, Container, Col, Row, Form, FormGroup, Input, Label, Button } from 'reactstrap';
import S3FileUpload from 'react-s3'
import jwt from 'jsonwebtoken'
import axios from 'axios'
import qs from 'qs'
import _ from 'lodash'
import moment from 'moment'
import AppConfig from '../../config/appConfig'
import { hashHistory } from 'react-router'
const decoded = jwt.decode(localStorage.token);

const config = {
  bucketName: 'mindtree1',
  dirName: 'audio', /* optional */
  region: 'us-east-2',
  accessKeyId: AppConfig.accesskey,
  secretAccessKey: AppConfig.secretKey,
}

class AddNewBeginners extends Component {
  constructor(props) {
    super(props)
    this.state = {
      input: {
        display: 'none'
      },
      custom: {
        border: '1px solid #ccc',
        display: 'inline - block',
        padding: '6px 12px',
        cursor: 'pointer'
      },
      banner: null,
      track: null,
      trackImage: null,
      loader: false,
      disable: false,
      load: false,
      loadr: false,
      audioList: [],
      name: '',
      name0: '',
      name1: '',
      name2: '',
      name3: '',
      name4: '',
      name5: '',
      name6: '',
      image: '',
      type: '',
      userData: {},
      inputs: [],
      cat: [],
      isLocked: true,
      audioData: [],
      resultData: {},
      selectOption: ["single", "multiple", "courses"]
    }

    this.submit = this.submit.bind(this)
    this.handleOnChange = this.handleOnChange.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  componentDidMount() {
    const { match } = this.props
    if (match && match.params && match.params.id) {

      axios.get(AppConfig.ApiUrl + `audio/${match.params.id}`, AppConfig.headerConfig).then(res => {
        if (res && res.data) {
          let selectOption = _.filter(this.state.selectOption, function (val) {
            return val !== res.data.type
          })

          this.setState({
            userData: (res && res.data) ? res.data : {},
            name: (res && res.data && res.data.name) ? res.data.name : "",
            audioData: (res && res.data && res.data.audio) ? res.data.audio : []
          })

          this.getCategoryData()
        }
      }).catch(err => {
        console.log(err)
      })
    }
  }

  handleChange(e) {
    if (e && e.length) {
      e.concat(e)

      this.setState({ cat: e })
    } else {
      this.setState({ cat: '' })
    }
  }
  _handleImageChange(e) {
    this.setState({ load: true, disable: true })
    var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
    const file = "bannerImage" + currentDate + e.target.files[0].name;
    var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
    var newFile = new File([filesObject], file, { type: e.target.files[0].type });

    S3FileUpload
      .uploadFile(newFile, config)
      .then(data => {

        this.setState({ banner: data.location, load: false, disable: false })
      })
      .catch(err => console.error(err))
  }

  _handleBgImageChange(e) {
    this.setState({ loadr: true, disable: true })
    var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
    const file = "bannerImage" + currentDate + e.target.files[0].name;
    var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
    var newFile = new File([filesObject], file, { type: e.target.files[0].type });

    S3FileUpload
      .uploadFile(newFile, config)
      .then(data => {
        this.setState({
          trackImage: data.location, loadr: false, disable: false
        })
      }).catch(err => {
        console.log(err, '------------')
      })
  }

  _handleAudioChange(e, index) {

    var imageList = []
    let result = {}
    let audioList = this.state.audioData
    this.setState({
      loader: true, disable: true
    })
    if (this.state.resultData && this.state.resultData.title) {
      audioList.concat(this.state.audioList)
    }
    var currentDate = moment().unix()
    const file = "beginners" + currentDate + e.target.files[0].name;
    var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
    var newFile = new File([filesObject], file, { type: e.target.files[0].type });
    S3FileUpload
      .uploadFile(newFile, config)
      .then(data => {

        result = audioList[index]
        result.audio = data.location
        audioList.concat(result)
        this.setState({
          audioData: audioList, loader: false, disable: false
        })
      }).catch(err => console.error(err))
    // })
    const image = this.state.audioList
    this.setState({ image })
  }
  handleOnChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }
  handleAudioTitleOnChange(e, index) {

    let data = this.state.audioData[index]
    data.title = e.target.value
    this.setState({
      resultData: data
    })
  }
  handleAudioSizeOnChange(e, index) {

    let data = this.state.audioData[index]
    data.size = e.target.value
    this.setState({
      resultData: data
    })
  }
  submit(event) {
    event.preventDefault()
    let payload = {
      name: this.state.name,
      bannerImage: (this.state.banner == null) ? this.state.userData.bannerImage : this.state.banner,
      trackImage: (this.state.trackImage == null) ? this.state.userData && this.state.userData.trackImage || "" : this.state.trackImage,
      audio: this.state.audioData && this.state.audioData.length && this.state.audioData || [],
      userId: decoded.id
    }

    const { match } = this.props
    if (match && match.params && match.params.id) {
      axios.patch(AppConfig.ApiUrl + `audio/${match.params.id}`, payload).then(res => {
        if (res) {
          toast.success("Updated sucessfully!")
          setTimeout(() => {
            hashHistory.push('/Beginners')
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
      })
    } else {
      axios.post(AppConfig.ApiUrl + `audio`, payload).then(res => {
        if (res && res.data) {
          toast.success("Beginners Added sucessfully!")
          setTimeout(() => {
            hashHistory.push('/Beginner')
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
      })
    }
  }

  appendInput() {
    var newInput = `input-${this.state.inputs.length}`;
    this.setState(prevState => ({ inputs: prevState.inputs.concat([newInput]) }));
  }

  render() {
    let _this = this
    return (
      <div className="app  align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8" xl="8">
              <Card className="mx-2">
                <CardBody className="p-4">
                  <Form onSubmit={this.submit} encType="multipart/form-data">

                    <FormGroup className="mb-3">
                      <Label><b>File Type</b></Label>
                      <Input type="select" name="type" id="exampleSelect" defaultValue={this.state.userData && this.state.userData.type} onChange={this.handleOnChange} required>
                        <option>{(this.state.userData && this.state.userData.type) ? this.state.userData.type : 'Session'}</option>
                        {
                          this.state.selectOption.map((option) =>
                            <option value={option}>{option}</option>
                          )
                        }
                      </Input>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label><b>Upload Banner Image</b></Label>
                      {
                        (this.state.load) ?
                          <Loader
                            type="TailSpin"
                            color="#00BFFF"
                            height={50}
                            width={50}
                            class="loader"
                            timeout={150000000}
                          />
                          : ""
                      }

                      <br></br>
                      <label for="file-upload1" class="custom-file-upload" style={_this.state.custom} >
                        <i class="fa fa-cloud-upload"></i> Choose file
                            </label>
                      <br></br>
                      <Input id="file-upload1" type="file" accept="image/*" style={_this.state.input} onChange={(e) => this._handleImageChange(e)} />
                      <br></br>
                      {
                        (this.state.userData && this.state.userData.bannerImage) ?
                          <img src={this.state.userData.bannerImage} style={{ height: '50px', width: '100px' }} alt="audio"></img>
                          : ""
                      }
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label><b>BackGround Image</b></Label>
                      {
                        (this.state.loadr) ?
                          <Loader
                            type="TailSpin"
                            color="#00BFFF"
                            height={50}
                            width={50}
                            class="loader"
                            timeout={150000000}
                          />
                          : ""
                      }
                      <br></br>
                      <label for="file-upload2" class="custom-file-upload" style={_this.state.custom} >
                        <i class="fa fa-cloud-upload"></i> Choose file
                            </label>
                      <br></br>
                      <Input id="file-upload2" type="file" accept="image/*" style={_this.state.input} onChange={(e) => this._handleBgImageChange(e)} />
                      {
                        (this.state.userData && this.state.userData.trackImage) ?
                          <img src={this.state.userData.trackImage} style={{ height: '50px', width: '100px' }} alt="audio"></img>
                          : ""
                      }
                    </FormGroup>
                    {
                      this.state.userData && this.state.userData.audio && this.state.userData.audio.length && this.state.userData.audio.map((val, index) =>
                        <div>
                          <FormGroup className="mb-3">
                            <Label><b>Track Title</b></Label>
                            <Input type="text" name="name" placeholder="Title" autoComplete="Name" required defaultValue={val && val.title || ""} onChange={(e) => { this.handleAudioTitleOnChange(e, index) }} />
                          </FormGroup>
                          <FormGroup className="mb-3">
                            <Label><b>Audio Duration</b></Label>
                            <Input type="text" name="size" placeholder="Audio Duration" autoComplete="Size" required defaultValue={val && val.size || ""} onChange={(e) => { this.handleAudioSizeOnChange(e, index) }} />
                          </FormGroup>
                          <FormGroup className="mb-3">

                            <Label><b>Upload Audio Files</b></Label>

                            <br></br>
                            <label for={"file-upload" + index * 2} class="custom-file-upload" style={_this.state.custom}  >
                              <i class="fa fa-cloud-upload"></i> Choose Audio
                            </label>
                            <br></br>
                            {val && val.audio && val.audio.substr(41, 55) || ""}
                            <Input id={"file-upload" + index * 2} type="file" accept="audio/*" style={_this.state.input} onChange={(e) => this._handleAudioChange(e, index)} />
                          </FormGroup>
                        </div>
                      )
                    }
                    {
                      (this.state.loader) ?
                        <Loader
                          type="TailSpin"
                          color="#00BFFF"
                          height={50}
                          width={50}
                          class="loader"
                          timeout={150000000}
                        />
                        : ""
                    }
                    <Button color="primary" class="btn btn-primary" disabled={(this.state.disable) ? true : false}>Submit</Button>

                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default AddNewBeginners;
