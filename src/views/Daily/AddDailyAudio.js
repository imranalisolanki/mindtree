import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import {Card, CardBody, Container, Col, Row,Form,FormGroup,Input,Label,Button } from 'reactstrap';
import S3FileUpload from 'react-s3'
import jwt from 'jsonwebtoken'
import axios from 'axios'
import moment from 'moment'
import AppConfig from '../../config/appConfig'
import { hashHistory } from 'react-router'
const decoded = jwt.decode(localStorage.token);

  const config = {
    bucketName: 'mindtree1',
    dirName: 'audio', /* optional */
    region: 'us-east-2',
    accessKeyId: AppConfig.accesskey,
    secretAccessKey: AppConfig.secretKey,
  }
class AddDailyAudio extends Component {
  constructor(props) {
    super(props)
    this.state = {
      image:[],
      banner:null,
      track:null,
      type:'',
      userData:{}
    }
    this.submit = this.submit.bind(this)
    this.handleOnChange = this.handleOnChange.bind(this)
  }
  
  componentDidMount(){
    const { match } = this.props
    if (match && match.params && match.params.id) {
      axios.get(AppConfig.ApiUrl + `audio/${match.params.id}`,AppConfig.headerConfig).then(res =>{
        if(res && res.data){
          this.setState({
            userData: (res && res.data) ? res.data : {},
            type: (res && res.data && res.data.type) ? res.data.type : ""
          })
        }
      }).catch(err =>{
        console.log(err)
      })
    }
  }
  _handleImageChange(e) {
      var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
      const file = "audioBannerImage" + currentDate + e.target.files[0].name;
      var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
      var newFile = new File([filesObject], file, { type: e.target.files[0].type });
      S3FileUpload
        .uploadFile(newFile, config)
        .then(data => {
          this.setState({
            banner: data.location
          })
        }).catch(err => {
          console.log(err,'------------')
        })
  }
  _handleAudioTrack(e) {
      var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
      const file = "audioBannerImage" + currentDate + e.target.files[0].name;
      var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
      var newFile = new File([filesObject], file, { type: e.target.files[0].type });
    // console.log(newFile,'-----------------')
      S3FileUpload
        .uploadFile(newFile, config)
        .then(data => {
          this.setState({
            track: data.location
          })
        }).catch(err => {
          console.log(err,'------------')
        })
  }
  _handleAudioChange(e){
    var imageList = []
    e.target.files.forEach(function (val, index) {
      var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
      const file = "userImage" + currentDate + e.target.files[index].name;
      var filesObject = e.target.files[index].slice(0, e.target.files[index].size, e.target.files[index].type);
      var newFile = new File([filesObject], file, { type: e.target.files[index].type });
      S3FileUpload
        .uploadFile(newFile, config)
        .then(data => {
          imageList.push(data.location)
        })
        .catch(err => console.error(err))
    })
    const image = imageList
    this.setState({ image })
  }
  handleOnChange(event){
    this.setState({
      type: event.target.value
    })
  }
  submit(event) {
    event.preventDefault()
    let payload = {
      name: event.target.name.value,
      day: Number(event.target.day.value),
      type: event.target.type.value,
      bannerImage: (this.state.banner === null) ? this.state.userData.bannerImage : this.state.banner,
      trackImage: (this.state.track === null) ? this.state.userData.bannerImage : this.state.track,
      audioFiles: (this.state.image && this.state.image.length) ? this.state.image : this.state.userData.audioFiles,
      userId: decoded.id,
      modiefy:moment().toISOString()
    }
    const { match } = this.props
    if (match && match.params && match.params.id){
      axios.patch(AppConfig.ApiUrl + `audio/${match.params.id}`, payload).then(res => {
        if (res) {
          toast.success("Updated sucessfully!")
          setTimeout(() => {
            hashHistory.push('/Audio')
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
      })
    }else{
      axios.post(AppConfig.ApiUrl + `audio`, payload).then(res => {
        if (res && res.data) {
          toast.success("Audio Added sucessfully!")
          setTimeout(() => {
            hashHistory.push('/Audio')
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
      })
    }
  }
  
  render() {
    return (
      <div className="app  align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8"  xl="8">
              <Card className="mx-2">
                <CardBody className="p-4">
                  <Form onSubmit={this.submit} encType="multipart/form-data">
                    <FormGroup className="mb-3">
                      <Label>Track Title</Label>
                      <Input type="text" name="name" placeholder="Title" autoComplete="Name" required defaultValue={this.state.userData && this.state.userData.name}/>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>Days it Appears</Label>
                      <Input type="text" name="day" placeholder="Day" autoComplete="Day" required defaultValue={this.state.userData && this.state.userData.day} />
                     
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>Session</Label>
                      <Input type="select" name="type" id="exampleSelect" defaultValue={this.state.userData && this.state.userData.type} onChange={this.handleOnChange} required>

                        <option>{(this.state.userData && this.state.userData.type) ? this.state.userData.type : 'Session'}</option>
                        <option value="single">Single</option>
                        <option value="multiple">Multiple</option>
                      </Input>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>Upload Banner Image</Label>
                      <Input type="file" placeholder="Image" accept="image/*" autoComplete="Image" onChange={(e) => this._handleImageChange(e)}/>
                      <br></br>
                      {
                      (this.state.userData && this.state.userData.bannerImage)?
                          <img src={this.state.userData.bannerImage} style={{height:'50px', width:'100px'}} alt="audio"></img>
                      :""
                      }
                    </FormGroup>
                    
                    <FormGroup className="mb-3">
                      <Label>Upload Track Imagee</Label>
                      <Input type="file" placeholder="Image" autoComplete="Image" accept="image/*" onChange={(e) => this._handleAudioTrack(e)} />
                      <Label>Upload Audio Files</Label>
                      <Input type="file" placeholder="Image" autoComplete="Image" accept="audio/*" onChange={(e) => this._handleAudioChange(e)} multiple />
                    </FormGroup>
                    
                    <Button color="primary" class="btn btn-primary">Submit</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default AddDailyAudio;
