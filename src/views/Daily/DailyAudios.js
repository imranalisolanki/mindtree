import React, { Component, Fragment } from 'react';
import S3FileUpload from 'react-s3'
// import Resizer from 'react-image-file-resizer';
import Loader from 'react-loader-spinner'
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import ReactAudioPlayer from 'react-audio-player'
import '../../scss/vendors/bootstrap-pagination.css';
import { Card, CardBody, CardHeader, Col, Badge } from 'reactstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Grid } from '@material-ui/core'
import { Button, TextField } from '@material-ui/core'
import { DialogContent, DialogContentText, Dialog, } from '@material-ui/core'
import FilterableTable from 'react-filterable-table'
import axios from 'axios'
import AppConfig from '../../config/appConfig'
import qs from 'qs'
import _ from 'lodash'
const getBadge = (status) => {
  return status === 'Active' ? 'success' :
    status === 'Inactive' ? 'secondary' :
      status === 'Pending' ? 'warning' :
        status === 'Banned' ? 'danger' :
          'primary'
}
const config = {
  bucketName: 'mindtree1',
  dirName: 'dailyImage', /* optional */
  region: 'us-east-2',
  accessKeyId: AppConfig.accesskey,
  secretAccessKey: AppConfig.secretKey,
}
class DailyAudios extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startAt: 0,
      currentPage: 1,
      activePage: 1,
      itemsPerPage: 10,
      limit: 10,
      skip: 0,
      loader: false,
      loade: false,
      disable: false,
      audioList: [],
      scroll: 'paper',
      open: false,
      audio: null,
      banner: null,
      track: null,
      type: '',
      name: '',
      title: '',
      day: 0,
      inputs: {},
      categoryList: [],
      cat: "",
      treeImage: null,
      message: "",
      editData: "",
      editId: null,
      load: false,
      size: null,
      error: null,
      er: null
    }
    this.handleOnChange = this.handleOnChange.bind(this)
    this._handleAudioChange = this._handleAudioChange.bind(this)
    this._handleBannerChange = this._handleBannerChange.bind(this)
  }
  componentDidMount() {
    const pageNumber = 1
    this.getDailyAudioListData(pageNumber)
  }
  handleClose() {
    this.setState({
      open: false, editData: "", error: null, er: null
    })

  }
  handleClickOpen(scrollType) {
    this.setState({
      open: true,
      scroll: scrollType,
      editData: ""
    })
  };

  getDailyAudioListData(pageNumber) {
    const { itemsPerPage } = this.state
    const activePage = pageNumber
    // const sortOrder = event
    this.setState({ activePage })
    const startAt = pageNumber * itemsPerPage - itemsPerPage
    var cond = {
      filter: {
        order: ['created DESC']
      }
    }
    const query = qs.stringify(cond, { addQueryPrefix: true })
    axios.get(AppConfig.ApiUrl + `dailies` + query, AppConfig.headerConfig).then(res => {
      if (res && res.data) {
        const audioList = (res && res.data) ? res.data : []
        this.setState({
          audioList, startAt
        })
      } else {
        this.setState({
          audioList: []
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }

  async _handleBannerChange(e) {
    this.setState({ loade: true, disable: true })

    var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
    const file = "treeImage" + currentDate + e.target.files[0].name;
    var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
    var newFile = new File([filesObject], file, { type: e.target.files[0].type });

    S3FileUpload
      .uploadFile(newFile, config)
      .then(data => {

        this.setState({ banner: data.location, loade: false, disable: false })
      })
      .catch(err => console.error(err))

    //   Resizer.imageFileResizer(
    //   e.target.files[0],
    //   220,
    //   300,
    //   'JPEG',
    //   95,
    //   0,
    //   uri => {
    //     var filesObject = uri
    //     var newFile = new File([filesObject], file, { type: uri.type });
    //     S3FileUpload
    //       .uploadFile(newFile, config)
    //       .then(data => {

    //         this.setState({ banner: data.location, loade: false, disable:false })
    //       })
    //       .catch(err => console.error(err))
    //   },
    //   'blob',
    //   200,
    //   300,
    // );

  }
  _handleAudioChange(e) {

    this.setState({ loader: true, disable: true })
    var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
    const file = "audio" + currentDate + e.target.files[0].name;
    var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
    var newFile = new File([filesObject], file, { type: e.target.files[0].type });
    S3FileUpload
      .uploadFile(newFile, config)
      .then(data => {

        this.setState({ audio: data.location, loader: false, disable: false })
      })
      .catch(err => console.error(err))

  }


  handleOnSubmit(event) {

    if (this.state.day && this.state.day != 0 || !this.state.banner || this.state.size != null) {


      let payload = {
        title: (this.state.title && this.state.title != '') ? this.state.title : this.state.editData.title,
        day: Number(this.state.day),
        size: this.state.size,
        message: this.state.message && this.state.message != '' && this.state.message || this.state.editData.message,
        bannerImage: this.state.banner,
        audio: (this.state.audio && this.state.audio != null) ? this.state.audio : this.state.editData && this.state.editData.audio || "",
        // treeImage: (this.state.treeImage && this.state.treeImage != null) ? this.state.treeImage : this.state.editData && this.state.editData.treeImage || "",
      }

      if (this.state.editId && this.state.editId != null) {
        axios.patch(AppConfig.ApiUrl + `dailies/${this.state.editId}`, payload).then(res => {
          if (res) {
            this.setState({ loader: false, day: 0, bannerImage: '' })
            toast.success("Updated sucessfully!")
            this.setState({ banner: null, audio: null, editData: "" })
            setTimeout(() => {
              this.getDailyAudioListData(1)
              this.handleClose()
            }, 1000);
          } else {
            this.setState({ loader: true })
          }
        }).catch(err => {
          console.log(err)
        })
      } else {
        axios.post(AppConfig.ApiUrl + `dailies`, payload).then(res => {
          if (res && res.data) {
            this.setState({ loader: false })
            toast.success("Daily Audio Added sucessfully!")
            setTimeout(() => {
              this.getDailyAudioListData(1)
              this.handleClose()
              this.setState({ banner: null, audio: null, error: null, er: null })
            }, 1000);
          } else {
            this.setState({ loader: true, er: "Fill all required fields!" })
          }
        }).catch(err => {
          console.log(err)
          this.setState({
            er: "Fill all required fields!"
          })
        })
      }
    } else {
      this.setState({
        error: "Fill all required fields!"
      })
    }

  }
  handleOnChange(e) {
    if (e.target.name === 'day') {
      if (e.target.value && e.target.value.length > 0) {
        this.state.error = null
      }
    }
    if (e.target.name === 'size') {
      if (e.target.value && e.target.value.length > 0) {
        this.state.error = null
      }
    }
    this.setState({
      [e.target.name]: e.target.value
    })

  }
  deleteAudioFile(id) {
    let cnd = window.confirm("Do you want delete Daily audio file?")
    if (cnd) {

      axios.delete(AppConfig.ApiUrl + `dailies/${id}`, AppConfig.headerConfig).then(res => {
        if (res) {
          toast.success("Deleted sucessfully!")
          this.setState({ loader: false })
          setTimeout(() => {
            this.getDailyAudioListData()
          }, 1000);
        } else {
          this.setState({ loader: true })
        }
      }).catch(err => {
        console.log(err)
      })
    } else {
      console.log('else part is here')
    }
  }
  EditAudioFile(scrollType, id) {
    this.setState({
      open: true,
      scroll: scrollType,
      editId: id
    })
    axios.get(AppConfig.ApiUrl + `dailies/${id}`, AppConfig.headerConfig).then(res => {
      if (res && res.data) {
        this.setState({
          editData: res && res.data || {},
          banner: res && res.data && res.data.bannerImage || {},
          day: res && res.data && res.data.day,
          size: res && res.data && res.data.size || "",
        })
      }
    }).catch(err => {
      console.log(err)
    })
  }
  render() {

    var _this = this
    var data = [];
    var pgNo = this.state.startAt;
    _.forEach(this.state.audioList, function (val, index) {
      data.push({

        name: val.title,
        day: (val && val.day) ? val.day : "",
        // banner: <img src={val.treeImage} style={{height:'50px', width:'25%'}} alt="ing"></img>,
        file: <ReactAudioPlayer
          src={(val && val.audio) ? val.audio : ""}
          controls
        />,
        message: val.message.slice(0, 15) + '......',
        action: <div><span onClick={() => { _this.EditAudioFile('paper', val.id) }} style={{ cursor: "pointer" }}><Badge color={getBadge('View')}>Edit</Badge></span> &nbsp;&nbsp; <span onClick={() => { _this.deleteAudioFile(val.id) }} style={{ cursor: "pointer" }}><Badge color={getBadge('Active')}>Delete</Badge></span></div >
      })
    })
    const fields = [
      { name: 'name', displayName: "Title", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'day', displayName: "Days", inputFilterable: true, sortable: true },
      { name: 'file', displayName: "Audio File", inputFilterable: false, exactFilterable: false, sortable: false, class: "test" },
      { name: 'message', displayName: "Daily Message", inputFilterable: false, exactFilterable: false, sortable: false, class: "test" },
      { name: 'action', displayName: "Action", inputFilterable: false, sortable: false },
    ];


    return (
      <Fragment >
        <Col xl={12}>

          <Card>
            <CardHeader>
              <i className="fa fa-align-justify"></i> Daily Audio <small className="text-muted">List</small>
              <button style={{ float: 'right' }} class="btn btn-primary" onClick={() => { this.handleClickOpen('paper') }}> Add New</button>
            </CardHeader>
            <CardBody>
              <FilterableTable
                namespace="People"
                initialSort="modify"
                topPagerVisible={false}
                data={data}
                fields={fields}
                noRecordsMessage="There are no Audio to display"
                noFilteredRecordsMessage="Audio not found!"
              />
            </CardBody>
          </Card>

        </Col>
        <Dialog
          open={this.state.open}
          // onClose={handleClose}
          scroll={this.state.scroll}
          className="modalWrapper"
        >
          <CardHeader>
            {/*  <Grid className="modalTitle"> */}
            <h3>Add Daily Audio</h3>
            {/* </Grid> */}
          </CardHeader>
          <DialogContent dividers={this.state.scroll === 'paper'}>
            <DialogContentText>
              <p style={{ color: 'red', textAlign: "center" }}>{this.state.er != null && this.state.er}</p>
              <Grid container spacing={3}>

                <Grid item sm={12} xs={12}>
                  <TextField
                    fullWidth
                    label="Title"
                    name="title"
                    key={this.state.editData && this.state.editData.title || ""}
                    defaultValue={this.state.editData && this.state.editData.title || ""}
                    required
                    className='inputField'
                    onChange={this.handleOnChange}
                  />
                </Grid>

                <Grid item sm={12} xs={12}>
                  <TextField
                    fullWidth
                    label="Day"
                    name="day"
                    key={this.state.editData && this.state.editData.day || ""}
                    defaultValue={this.state.editData && this.state.editData.day || ""}
                    required
                    className='inputField'
                    error={this.state.error && true}
                    helperText={this.state.error}
                    onChange={this.handleOnChange}
                  />
                </Grid>

                <Grid item sm={12} xs={12}>
                  <TextField
                    fullWidth
                    label="Audio Duration"
                    name="size"
                    key={this.state.editData && this.state.editData.size || ""}
                    defaultValue={this.state.editData && this.state.editData.size || ""}
                    required
                    className='inputField'
                    error={this.state.error && true}
                    helperText={this.state.error}
                    onChange={this.handleOnChange}
                  />
                </Grid>

                {/*  <Grid item sm={12} xs={12}>
                  <label>Upload banner Image</label>
                  <input type="file" placeholder="Image" autoComplete="Image" accept="image/*" onChange={(e) => this._handleBannerChange(e)} />
                </Grid> */}
                <Grid item sm={6} xs={12}>
                  <label>Upload Audio File</label>
                  {
                    (this.state.loader) ?
                      <Loader
                        type="TailSpin"
                        color="#00BFFF"
                        height={50}
                        width={50}
                        class="loader"
                        timeout={150000000}
                      />
                      : ""
                  }
                  <input type="file" placeholder="Image" autoComplete="Image" accept="audio/*" onChange={(e) => this._handleAudioChange(e)} />
                </Grid>
                <Grid item sm={6} xs={12}>
                  <label>Upload banner Image</label>
                  {
                    (this.state.loade) ?
                      <Loader
                        type="TailSpin"
                        color="#00BFFF"
                        height={50}
                        width={50}
                        class="loader"
                        timeout={150000000}
                      />
                      : ""
                  }
                  <input type="file" placeholder="Image" autoComplete="Image" accept="image/*" onChange={(e) => this._handleBannerChange(e)} />

                </Grid>
                <Grid item xs={12}>
                  <TextField
                    label="Audio Message"
                    fullWidth
                    required
                    multiline
                    key={this.state.editData && this.state.editData.message || ""}
                    defaultValue={this.state.editData && this.state.editData.message || ""}
                    rows="4"
                    name="message"
                    className='inputField inputFieldFilled'
                    variant="filled"
                    onChange={this.handleOnChange}
                  />
                </Grid>

              </Grid>


            </DialogContentText>
          </DialogContent>
          <Grid className="modalFooter">
            <Button className="bg-warning" onClick={() => { this.handleClose() }} disabled={(this.state.disable) ? true : false}> Close </Button>

            <Button className="btn bg-default" onClick={() => { this.handleOnSubmit() }}
              disabled={(this.state.disable) ? true : false}> Save </Button>

          </Grid>
        </Dialog>
        <ToastContainer />
      </Fragment>

    )
  }
}

export default DailyAudios;
