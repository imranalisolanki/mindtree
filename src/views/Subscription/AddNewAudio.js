import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import {Card, CardBody, Container, Col, Row,Form,FormGroup,Input,Label,Button } from 'reactstrap';
import S3FileUpload from 'react-s3'
import jwt from 'jsonwebtoken'
import axios from 'axios'
import moment from 'moment'
import AppConfig from '../../config/appConfig'
import { hashHistory } from 'react-router'
const decoded = jwt.decode(localStorage.token);

  const config = {
    bucketName: 'greenwood-app',
    dirName: 'users', /* optional */
    region: 'us-east-2',
    accessKeyId: 'AKIA2WAK2A7P5BP63ZH3',
    secretAccessKey: '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',
  }
class AddNewAudio extends Component {
  constructor(props) {
    super(props)
    this.state = {
      image:[],
      banner:null,
      type:'',
      userData:{}
    }
    this.submit = this.submit.bind(this)
    this.handleOnChange = this.handleOnChange.bind(this)
  }
  
  componentDidMount(){
    const { match } = this.props
    if (match && match.params && match.params.id) {
      axios.get(AppConfig.ApiUrl + `audio/${match.params.id}`,AppConfig.headerConfig).then(res =>{
        if(res && res.data){
          this.setState({
            userData: (res && res.data) ? res.data : {},
            type: (res && res.data && res.data.type) ? res.data.type : ""
          })
        }
      }).catch(err =>{
        console.log(err)
      })
    }
  }
  _handleImageChange(e) {
      var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
      const file = "audioBannerImage" + currentDate + e.target.files[0].name;
      var filesObject = e.target.files[0].slice(0, e.target.files[0].size, e.target.files[0].type);
      var newFile = new File([filesObject], file, { type: e.target.files[0].type });
      S3FileUpload
        .uploadFile(newFile, config)
        .then(data => {
          this.setState({
            banner: data.location
          })
        }).catch(err => {
          console.log(err,'------------')
        })
  }
  _handleAudioChange(e){
    var imageList = []
    e.target.files.forEach(function (val, index) {
      var currentDate = Math.floor(Math.random() * 1000 * 10000 * 10000)
      const file = "userImage" + currentDate + e.target.files[index].name;
      var filesObject = e.target.files[index].slice(0, e.target.files[index].size, e.target.files[index].type);
      var newFile = new File([filesObject], file, { type: e.target.files[index].type });
      S3FileUpload
        .uploadFile(newFile, config)
        .then(data => {
          imageList.push(data.location)
        })
        .catch(err => console.error(err))
    })
    const image = imageList
    this.setState({ image })
  }
  handleOnChange(event){
    this.setState({
      type: event.target.value
    })
  }
  submit(event) {
    event.preventDefault()
    let payload = {
      name: event.target.name.value,
      day: Number(event.target.day.value),
      type: event.target.type.value,
      bannerImage: (this.state.banner === null) ? this.state.userData.bannerImage : this.state.banner,
      audioFiles: (this.state.image && this.state.image.length) ? this.state.image : this.state.userData.audioFiles,
      userId: decoded.id,
      modiefy:moment().toISOString()
    }
    const { match } = this.props
    if (match && match.params && match.params.id){
      axios.patch(AppConfig.ApiUrl + `audio/${match.params.id}`, payload).then(res => {
        if (res) {
          toast.success("Updated sucessfully!")
          setTimeout(() => {
            hashHistory.push('/Audio')
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
      })
    }else{
      axios.post(AppConfig.ApiUrl + `audio`, payload).then(res => {
        if (res && res.data) {
          toast.success("Audio Added sucessfully!")
          setTimeout(() => {
            hashHistory.push('/Audio')
          }, 1000);
        }
      }).catch(err => {
        console.log(err)
      })
    }
  }
  
  render() {
    return (
      <div className="app  align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8"  xl="8">
              <Card className="mx-2">
                <CardBody className="p-4">
                  <Form onSubmit={this.submit} encType="multipart/form-data">
                    <FormGroup className="mb-3">
                      <Label>Name</Label>
                      <Input type="text" name="name" placeholder="Name" autoComplete="Name" required defaultValue={this.state.userData && this.state.userData.name}/>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>No Of Day</Label>
                      <Input type="select" name="day" id="exampleSelect" defaultValue={this.state.userData && this.state.userData.day}>
                        <option>{(this.state.userData && this.state.userData.day) ? this.state.userData.day : 'Choose Day'}</option>
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                      <option>6</option>
                      <option>7</option>
                    </Input>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>Banner Image</Label>
                      <Input type="file" placeholder="Image" accept="image/*" autoComplete="Image" onChange={(e) => this._handleImageChange(e)}/>
                      <br></br>
                      {
                      (this.state.userData && this.state.userData.bannerImage)?
                          <img src={this.state.userData.bannerImage} style={{height:'50px', width:'100px'}} alt="audio"></img>
                      :""
                      }
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>Lession Type</Label>
                      <Input type="select" name="type" id="exampleSelect" defaultValue={this.state.userData && this.state.userData.type} onChange={this.handleOnChange} required>
                      
                        <option>{(this.state.userData && this.state.userData.type) ? this.state.userData.type: 'Selelct Type'}</option>
                        <option value="single">single</option>
                        <option value="multiple">Multiple</option>
                      </Input>
                    </FormGroup>
                       {/* {
                      (this.state.type === 'single')?
                        <FormGroup className="mb-3">
                          <Label>Choose Audio File</Label>
                          <Input type="file" placeholder="Image" autoComplete="Image" accept="audio/*" onChange={(e) => this._handleAudioChange(e)} />
                        </FormGroup>
                        : (this.state.type === 'multiple') ? <FormGroup className="mb-3">
                          <Label>Files Name</Label>
                          <Input type="text" name="fi" placeholder="Files Name" autoComplete="Files Name" />
                          <Label>Choose Audio Files</Label>
                          <Input type="file" placeholder="Image" autoComplete="Image" accept="audio/*" onChange={(e) => this._handleAudioChange(e)} multiple />
                        </FormGroup>
                        :""
                       } */}
                    
                    <Button color="primary" class="btn btn-primary">Submit</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default AddNewAudio;
