import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../../scss/vendors/bootstrap-pagination.css';
import {Card, CardBody, CardHeader, Col, Row, Badge } from 'reactstrap';
import FilterableTable from 'react-filterable-table'
import axios from 'axios'
import AppConfig from '../../config/appConfig'
import qs from 'qs'
import _ from 'lodash'
import moment from 'moment';
const getBadge = (status) => {
  return status === 'Active' ? 'success' :
    status === 'Inactive' ? 'secondary' :
      status === 'Pending' ? 'warning' :
        status === 'Banned' ? 'danger' :
          'primary'
}
class Subscription extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startAt: 0,
      currentPage: 1,
      activePage: 1,
      itemsPerPage: 10,
      limit:10,
      skip:0,
      audioList:[]
    }
  }
  componentDidMount() {
    const pageNumber = 1
    this.getAudioListData(pageNumber)
  }

  getAudioListData(pageNumber) {
    const { itemsPerPage } = this.state
    const activePage = pageNumber
    // const sortOrder = event
    this.setState({ activePage })
    const startAt = pageNumber * itemsPerPage - itemsPerPage
    var cond = {
      filter: {
        include:[
         { relation:"user"}
        ],
        order:['created DESC']
      }
    }
    const query = qs.stringify(cond, { addQueryPrefix: true })
    axios.get(AppConfig.ApiUrl + `subscriptions` + query, AppConfig.headerConfig).then(res => {
      if(res && res.data){
        const audioList = (res && res.data) ? res.data : []
        this.setState({
          audioList, startAt
        })
      }else {
        this.setState({
          audioList:[]
        }) 
      }
    }).catch(err =>{
      console.log(err)
    })
  }
  render() {
    var data = [];
    var pgNo = this.state.startAt;
    _.forEach(this.state.audioList, function (val, index) {
      data.push({
        Id: pgNo + index + 1,
        planType: val.planType,
        price: (val && val.price) ? val.price : "",
        name: (val && val.user && val.user.name) ? val.user.name : "",
        creater: moment(val.created).format('YYYY-MM-DD'),
       
      })
    })
    const fields = [
      { name: 'Id', displayName: "Id", inputFilterable: true, sortable: true },
      { name: 'planType', displayName: "Type", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'price', displayName: "Price", inputFilterable: false, exactFilterable: false, sortable: false,class:"test" },
      { name: 'name', displayName: "User Name", inputFilterable: true, sortable: true },
      { name: 'creater', displayName: "Creater", inputFilterable: true, sortable: true },
     
    ];
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Subscription <small className="text-muted">List</small>
              </CardHeader>
              <CardBody>
                <FilterableTable
                  namespace="People"
                  initialSort="modify"
                  topPagerVisible={false}
                  data={data}
                  fields={fields}
                  noRecordsMessage="There are no Subscription to display"
                  noFilteredRecordsMessage="Subscription not found!"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Subscription;
