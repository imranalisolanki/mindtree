import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {Badge, Card, CardBody, CardHeader, Col, Row, Table, Input, FormGroup, Button } from 'reactstrap';
import config from '../../config/appConfig'
import axios from 'axios'

const getBadge = (status) => {
  return status === 'Active' ? 'success' :
    status === 'Inactive' ? 'secondary' :
      status === 'Pending' ? 'warning' :
        status === 'Banned' ? 'danger' :
          'primary'
}

class PurchaseOrder extends Component {
constructor(props) {
  super(props)
  this.state = {
    taskList:[],
    document_number:0,
    even: 1
  }
}
componentDidMount(){
  this.getTaskList()
}
getTaskList(dNumber){
  var query = {}
  if (dNumber && dNumber > 0){
    query = {
      or: [
        {document_number: dNumber },
        {purchaseOrder_number: dNumber }
      ]
    }
   
  }
  var cond = {
    where:query,
    skip:0,
    limit:20
  }
  cond = JSON.stringify(cond);
  axios.get(config.ApiUrl +`tasks?filter=`+cond).then(res =>{
    if(res){
      const taskList = (res && res.data) ? res.data : []
      this.setState({ taskList})
    }
  })
}
  searchDocumentNumber(e){
    if (e.target.value && e.target.value.length > 1){
      const document_number = e.target.value
      this.setState({ document_number })
      this.getTaskList(e.target.value)
    }else{
      const document_number = 0
      this.setState({ document_number })
      this.getTaskList(e.target.value) 
    }
    
  }
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={8} sm={{ offset: 2 }}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Purchase Order <small className="text-muted">List</small>
                <Link to={`/PurchaseOrder/AddNewPurchaseOrder`} style={{ float: 'right' }}>
                  <Button color="success">Add New Purchase Order</Button>
                </Link>
              </CardHeader>
              <Col xl={12} style={{ padding: '14px' }}>
                <Col xl={8} style={{ float: 'left' }}>
                  <FormGroup>
                    <Input type="number" id="name" placeholder="Search By Document Number/Purchase Order Number" onChange={(e) => { this.searchDocumentNumber(e) }} autoComplete="off" />
                  </FormGroup>
                </Col>
              </Col>
              <CardBody>
                
                    {
                  (this.state.taskList && this.state.taskList.length)?
                      this.state.taskList.map((task, index) =>
                      <div>
                          <Table responsive hover className={((this.state.even+index)%2)?"even":"odd"}>
                        <tbody>
                            <tr><th><b>{index + 1}.</b>  Line Number</th> 
                                <td>{task.Items.number}   <Link to={`/PurchaseOrder/EditPurchaseOrder/${task.id}`} style={{float:'right'}}>
                                  <Badge color={getBadge('Pending')}>Edit</Badge>
                                </Link> </td>
                        </tr>
                          <tr><th>Document Number</th>
                            <td>  {task.document_number}</td></tr>
                          <tr>
                            <th scope="col">Material  </th>
                            <td>{task.type}</td>
                          </tr>
                          <tr>
                            <th scope="col">EAN  </th>
                            <td>{task.Items.EAN}</td>
                          </tr>
                          <tr>
                            <th scope="col">Quantity  
                            </th>
                            <td>{task.Items.gr_quantity}/{task.Items.order_quantity}</td>
                            </tr>

                            <tr>
                            <th scope="col">Material Description </th>
                            <td>{task.Items.material_description}</td>
                          </tr>
                          </tbody>
                          </Table>
                          <Table></Table>
                      </div>
                        
                    ) : <span className="notfound"><b>Sorry !</b><br></br>Data Not Found</span>
                    }
                  
               
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default PurchaseOrder;
