import React, { Component } from 'react';
import { Card, CardBody, Col, Row, FormGroup, Input, Label, Button, Form, Container } from 'reactstrap';
import config from '../../config/appConfig'
import axios from 'axios'
import { hashHistory } from 'react-router'

class AddNewPurchaseOrder extends Component {
  constructor(props){
    super(props);
    this.state = {
      taskDetails:''
    }
    this.submit = this.submit.bind(this)
  }
componentDidMount(){
  if (this.props && this.props.match && this.props.match.params && this.props.match.params.id) {
    this.getTaskById(this.props.match.params.id)
  }
}
getTaskById(id){
  axios.get(config.ApiUrl +`tasks/`+id).then(res =>{
    if(res){
      const taskDetails = (res && res.data) ? res.data:{}
      this.setState({ taskDetails})
    }
  })
}
  submit(e){
    e.preventDefault()
    if (this.props && this.props.match && this.props.match.params && this.props.match.params.id) {
      var data = {
        type: e.target[0].value,
        document_number: e.target[2].value,
        Items: {
          number: e.target[3].value,
          material_description: e.target[1].value,
          EAN: e.target[4].value,
          order_quantity: e.target[5].value,
          gr_quantity: e.target[6].value,
          storage_location: e.target[7].value,
        },
        purchaseOrder_number: e.target[8].value
      }
      axios.patch(config.ApiUrl + `tasks/` + this.props.match.params.id, data).then(res =>{
        if(res){
          setTimeout(() => {
            hashHistory.push('/PurchaseOrder/ViewPurchaseOrderList')
          }, 1000);
        }
      })
    }else{
      var datas = {
        type: e.target[0].value,
        document_number: e.target[2].value,
        Items: {
          number: e.target[3].value,
          material_description: e.target[1].value,
          EAN: e.target[4].value,
          order_quantity: e.target[5].value,
          gr_quantity: e.target[6].value,
          storage_location: e.target[7].value,
        },
        purchaseOrder_number: e.target[8].value
      }

      axios.post(config.ApiUrl +`tasks`,datas).then(res =>{
        if(res){
          setTimeout(() => {
            hashHistory.push('/PurchaseOrder/ViewPurchaseOrderList')
          }, 1000);
        }
      })
    }
  }
  render() {
    return (
      <div className="app  align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8" lg="3" xl="8">
              <Card className="mx-2">
                <CardBody className="p-4">
                  <Form onSubmit={this.submit} encType="multipart/form-data">
                    <FormGroup className="mb-3">
                      <Label>Material</Label>
                      <Input type="text" placeholder="Material" autoComplete="Title" required defaultValue={this.state.taskDetails.type}/>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label> Material Description</Label>
                      <Input type="text" placeholder="Description" autoComplete="Description" defaultValue={this.state.taskDetails && this.state.taskDetails.Items && this.state.taskDetails.Items.material_description} />
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>Document number</Label>
                      <Input type="number" placeholder="Document number" autoComplete="Document number" required defaultValue={this.state.taskDetails.document_number}/>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>Line Number</Label>
                      <Input type="number" placeholder="Items Number" autoComplete="Items Number" required defaultValue={this.state.taskDetails && this.state.taskDetails.Items && this.state.taskDetails.Items.number}/>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>EAN Number</Label>
                      <Input type="number" placeholder="EAN Number" autoComplete="EAN Number" required defaultValue={this.state.taskDetails && this.state.taskDetails.Items && this.state.taskDetails.Items.EAN}/>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>Order Quantity</Label>
                      <Input type="number" placeholder="Order Quantity" autoComplete="Order Quantity" required defaultValue={this.state.taskDetails && this.state.taskDetails.Items && this.state.taskDetails.Items.order_quantity}/>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>Group Quantity</Label>
                      <Input type="number" placeholder="Group Quantity" autoComplete="Group Quantity" required defaultValue={this.state.taskDetails && this.state.taskDetails.Items && this.state.taskDetails.Items.gr_quantity} />
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>Storage Location</Label>
                      <Input type="number" placeholder="Storage Location" autoComplete="Storage Location" required defaultValue={this.state.taskDetails && this.state.taskDetails.Items && this.state.taskDetails.Items.storage_location}/>
                    </FormGroup>
                    <FormGroup className="mb-3">
                      <Label>Purchase Order Number</Label>
                      <Input type="number" placeholder="Purchase Order Number" autoComplete="Purchase Order Number" required defaultValue={this.state.taskDetails && this.state.taskDetails.Items && this.state.taskDetails.Items.gr_quantity} />
                    </FormGroup>
                    <Button color="success">Submit</Button>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default AddNewPurchaseOrder;
