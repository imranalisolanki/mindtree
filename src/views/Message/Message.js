import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import '../../scss/vendors/bootstrap-pagination.css';
import { Card, CardBody, CardHeader, Col, Row, Badge } from 'reactstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Grid } from '@material-ui/core'
import { Button, TextField } from '@material-ui/core'
// import Dropzone from 'react-dropzone-uploader';
import { DialogContent, DialogContentText, Dialog, } from '@material-ui/core'
import FilterableTable from 'react-filterable-table'
import jsonWebToken from 'jsonwebtoken'
import axios from 'axios'
import AppConfig from '../../config/appConfig'
import qs from 'qs'
import _ from 'lodash'
// import moment from 'moment';
const getBadge = (status) => {
  return status === 'Active' ? 'success' :
    status === 'Inactive' ? 'secondary' :
      status === 'Pending' ? 'warning' :
        status === 'Banned' ? 'danger' :
          'primary'
}
class Plans extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startAt: 0,
      currentPage: 1,
      scroll: 'paper',
      open: false,
      activePage: 1,
      itemsPerPage: 10,
      limit: 10,
      skip: 0,
      messageList: [],
      message: "",
      er: null,
      editId: '',
      editData: {},
      userId: ""
    }
    this.handleOnChangeName = this.handleOnChangeName.bind(this)
  }
  componentDidMount() {
    const pageNumber = 1
    this.getMessageListData(pageNumber)
    let token = jsonWebToken.decode(AppConfig.token)
    this.setState({ userId: token && token.id || "" })
  }
  handleOnChangeName(e) {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleClose() {
    this.setState({
      open: false,
      er: null
    })
  }

  getMessageListData(pageNumber) {
    const { itemsPerPage } = this.state
    const activePage = pageNumber

    this.setState({ activePage })
    const startAt = pageNumber * itemsPerPage - itemsPerPage
    var cond = {
      filter: { order: ['created DESC'] }
    }
    const query = qs.stringify(cond, { addQueryPrefix: true })

    axios.get(AppConfig.ApiUrl + `medication-messages` + query, AppConfig.headerConfig).then(res => {
      if (res && res.data) {
        const messageList = (res && res.data) ? res.data : []
        this.setState({ messageList, startAt })
      } else {
        this.setState({ messageList: [] })
      }
    }).catch(err => {
      console.log(err)
    })
  }

  handleOnSubmit(event) {

    let message = {
      message: this.state.message,
      userId: this.state.userId
    }
    if (this.state.message && this.state.message.length > 1) {
      if (this.state.editId && this.state.editId !== undefined) {
        axios.patch(AppConfig.ApiUrl + `medication-messages/` + this.state.editId, message, AppConfig.headerConfig).then(res => {
          if (res) {
            toast.success("Meditation Messages Updated sucessfully!")
            this.setState({ er: null, message: "" })
            setTimeout(() => {
              this.getMessageListData(1)
              this.handleClose()
            }, 1000);
          }
        }).catch(err => {
          this.setState({
            er: "All Fields Are Mandatory"
          })
          console.log(err)
        })
      } else {
        axios.post(AppConfig.ApiUrl + `medication-messages`, message, AppConfig.headerConfig).then(res => {
          if (res) {
            toast.success("Meditation Messages Added sucessfully!")
            this.setState({ er: null, message: "" })
            setTimeout(() => {
              this.getMessageListData(1)
              this.handleClose()
            }, 1000);
          }
        }).catch(err => {
          this.setState({
            er: "All Fields Are Mandatory"
          })
          console.log(err)
        })
      }
    } else {
      this.setState({ er: "Medication Message is required!" })
    }
  }

  handleClickOpen(scrollType, id) {
    if (id && id != undefined) {
      let response = _.filter(this.state.messageList, function (val) {
        return val.id === id
      })
      this.setState({
        editId: id,
        editData: response && response[0] || {},
        message: response && response[0] && response[0].message || ""
      })
    } else {
      this.setState({
        editData: ""
      })
    }
    this.setState({
      open: true,
      scroll: scrollType
    })
  };
  render() {
    let _this = this
    var data = [];
    _.forEach(this.state.messageList, function (val, index) {
      data.push({
        message: val.message,
        created: val && val.created && val.created.substr(0, 10) || "",
        modified: val && val.modified && val.modified.substr(0, 10) || "",
        action: <span style={{ cursor: 'pointer' }} onClick={() => { _this.handleClickOpen('paper', val.id) }}><Badge color={getBadge('Active')}>Edit</Badge></span>

      })
    })
    const fields = [
      { name: 'message', displayName: "Message", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'created', displayName: "Created", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'modified', displayName: "Modified", inputFilterable: true, exactFilterable: true, sortable: true },
      { name: 'action', displayName: "Action", inputFilterable: false, sortable: false },

    ];
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Meditation Message <small className="text-muted">List</small>
                <button class="btn btn-primary" style={{ float: "right" }} onClick={() => { this.handleClickOpen('paper') }}> Add New</button>
              </CardHeader>
              <CardBody>
                <FilterableTable
                  namespace="Message"
                  initialSort="modify"
                  topPagerVisible={false}
                  data={data}
                  fields={fields}
                  noRecordsMessage="There are no message to display"
                  noFilteredRecordsMessage="Message not found!"
                />
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Dialog
          open={this.state.open}
          // onClose={handleClose}
          scroll={this.state.scroll}
          className="modalWrapper"
        >
          <CardHeader>
            {/*  <Grid className="modalTitle"> */}
            <h3>Add New Message</h3>
            {/* </Grid> */}
          </CardHeader>
          <DialogContent dividers={this.state.scroll === 'paper'}>
            <DialogContentText>
              <Grid container spacing={3}>

                <Grid item sm={12} xs={12}>

                  <TextField
                    placeholder="Medication Message"
                    fullWidth
                    multiline
                    required={true}
                    rows="6"
                    key={"225225"}
                    defaultValue={this.state.message}
                    name="message"
                    className='inputField inputFieldFilled'
                    variant="filled"
                    error={this.state.er && true}
                    helperText={this.state.er && this.state.er}
                    onChange={this.handleOnChangeName}
                  />
                </Grid>
              </Grid>
            </DialogContentText>
          </DialogContent>
          <Grid className="modalFooter">
            <Button className="bg-warning" onClick={() => { this.handleClose() }}> Close </Button>
            <Button className="btn bg-default" onClick={() => { this.handleOnSubmit() }}> Save </Button>

          </Grid>
        </Dialog>
        <ToastContainer />
      </div>
    )
  }
}

export default Plans;
